package gofumo

import (
	"fmt"
	"math"
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Command represents what gofumo thinks is a concise command format
// I made help and commands return an array because sometimes you want related commands
// so instead of making two handlers, you can put them in one command which switches
// but the help will display as two commands
// SimpleCommand below just allows one, but ComplexCommand will allow this
type Command interface {
	// this should check the prefix and stuff to make sure message matches
	Handler() EventHandler
	// help strings that'll appear in the help msg and describe what the cmd does
	Help() []string
	// actual command string that'll be searched with any needed args
	Commands() []string
	// does it need the 1 char prefix?
	RequiresCharPrefix() bool
	CharPrefix() string
	// does it need to have module name?
	RequiresModulePrefix() bool
	// what permissions are needed to use?
	PermissionsNeeded() int
}

// SimpleCommand is a plain implementation of a struct satisfying the Command interface
type SimpleCommand struct {
	EventHdlr EventHandler

	HelpString    string
	CommandString string

	NeedsCharPrefix   bool
	NeedsModulePrefix bool

	Permissions int
}

// Handler returns the internal handler
func (sc *SimpleCommand) Handler() EventHandler {
	return sc.EventHdlr
}

// Help returns the internal help string
func (sc *SimpleCommand) Help() []string {
	return []string{sc.HelpString}
}

// Commands returns the internal command string
func (sc *SimpleCommand) Commands() []string {
	return []string{sc.CommandString}
}

// RequiresCharPrefix returns if the char prefix should be required
func (sc *SimpleCommand) RequiresCharPrefix() bool {
	return sc.NeedsCharPrefix
}

// CharPrefix returns the default prefix for the bot for a simple command
func (sc *SimpleCommand) CharPrefix() string {
	return Prefix
}

// RequiresModulePrefix returns if the module named is required to be prefixed
func (sc *SimpleCommand) RequiresModulePrefix() bool {
	return sc.NeedsModulePrefix
}

// PermissionsNeeded returns Permissions needed for command
func (sc *SimpleCommand) PermissionsNeeded() int {
	return sc.Permissions
}

// CommandChecker is a utility function that will be passed into an EventHandlerGenerator
// Will check to see if any commands string with the proper prefixes match the message string
type CommandChecker func(p Plugin, m *discordgo.MessageCreate) bool

// BotIDChecker will make sure bot and author ids don't match
type BotIDChecker func(s *discordgo.Session, m *discordgo.MessageCreate) bool

// PermissionChecker is another utility function that will be passed into an EventHandlerGenerator
// Will check to see if a user has the correct permissions as specified in the command
type PermissionChecker func(s *discordgo.Session, m *discordgo.MessageCreate) bool

// EventHandlerGenerator is a decent way to get two utility functions for checking your commands
type EventHandlerGenerator func(BotIDChecker, CommandChecker, PermissionChecker) EventHandler

const (
	needsCharAndModulePrefixFormatString = "%s%s %s"
	needsModulePrefixFormatString        = "%s %s"
	needsCharPrefixFormatString          = "%s%s"
	needsNoPrefixFormatString            = "%s"
)

// CommandSyntax is a shorthand for creating multisub command
// 3-length array that contains command name, arg usage, and help
// command name is what will be used to check in the helper function CommandChecker
// AND displayed to users using the help command
// args and help will only be used to display to users currently
type CommandSyntax [3]string

// Command returns the 1st element, which should be the command alias so to speak
func (c *CommandSyntax) Command() string {
	return c[0]
}

// Args returns the 2nd element, which should be a nice looking arg format for the user
func (c *CommandSyntax) Args() string {
	return c[1]
}

// Help returns the 3rd element which should be a description of the command
func (c *CommandSyntax) Help() string {
	return c[2]
}

// MultiSubCommand allows a command to have several subcommands tied to one handler
type MultiSubCommand struct {
	EventHdlr EventHandler

	HelpStrings       []string
	CommandStrings    []string
	CommandArgStrings []string

	Prefix string

	NeedsCharPrefix   bool
	NeedsModulePrefix bool

	Permissions int
}

// Handler returns the internal handler
func (sc *MultiSubCommand) Handler() EventHandler {
	return sc.EventHdlr
}

// Help returns the internal help string
func (sc *MultiSubCommand) Help() []string {
	return sc.HelpStrings
}

// Commands returns the internal command string
func (sc *MultiSubCommand) Commands() []string {
	commands := make([]string, len(sc.CommandStrings))
	for idx := range commands {
		commands[idx] = fmt.Sprintf("%s %s", sc.CommandStrings[idx], sc.CommandArgStrings[idx])
	}
	return commands
}

// CharPrefix returns the configured char prefix. Only valid if requiresCharPrefix returns true
func (sc *MultiSubCommand) CharPrefix() string {
	return sc.Prefix
}

// RequiresCharPrefix returns if the char prefix should be required
func (sc *MultiSubCommand) RequiresCharPrefix() bool {
	return sc.NeedsCharPrefix
}

// RequiresModulePrefix returns if the module named is required to be prefixed
func (sc *MultiSubCommand) RequiresModulePrefix() bool {
	return sc.NeedsModulePrefix
}

// PermissionsNeeded returns Permissions needed for command
func (sc *MultiSubCommand) PermissionsNeeded() int {
	return sc.Permissions
}

func NewMultiSubCommand(
	commands []CommandSyntax,
	charPrefix string,
	needsModulePrefix bool,
	permissions int,
	handlerGenerator EventHandlerGenerator) *MultiSubCommand {

	commandStrings := make([]string, len(commands))
	commandArgStrings := make([]string, len(commands))
	helpStrings := make([]string, len(commands))
	for idx := range commands {
		commandStrings[idx] = commands[idx].Command()
		commandArgStrings[idx] = commands[idx].Args()
		helpStrings[idx] = commands[idx].Help()
	}

	command := &MultiSubCommand{
		HelpStrings:       helpStrings,
		CommandStrings:    commandStrings,
		CommandArgStrings: commandArgStrings,

		Prefix: charPrefix,

		NeedsCharPrefix:   charPrefix != "",
		NeedsModulePrefix: needsModulePrefix,

		Permissions: permissions,

		EventHdlr: handlerGenerator(
			createBotIDChecker(),
			createCommandChecker(needsModulePrefix, charPrefix, commandStrings),
			createPermissionChecker(permissions),
		),
	}
	return command
}

// this is a bit of a doozy
// so we have 4 possible formatting directives we need to account for
// I could put all the funcs into 1 switch inside the commandchecker closure
// but I believe this way it's only created once and checked once
// the other way would be checked multiple times for each correct formatting directive
func createCommandChecker(needsModulePrefix bool, charPrefix string, commandStrings []string) CommandChecker {
	singleCommandCheck := func(module, command, message string) bool {
		var prefix string
		needsCharPrefix := charPrefix != ""
		switch {
		case needsModulePrefix && needsCharPrefix:
			prefix =
				fmt.Sprintf(needsCharAndModulePrefixFormatString, charPrefix, module, command)
		case needsModulePrefix:
			prefix =
				fmt.Sprintf(needsModulePrefixFormatString, module, command)
		case needsCharPrefix:
			prefix =
				fmt.Sprintf(needsCharPrefixFormatString, charPrefix, command)
		default:
			prefix =
				fmt.Sprintf(needsNoPrefixFormatString, command)
		}
		return strings.HasPrefix(message, prefix)
	}

	// finally actually make the function
	// if anything is a correct hit, then go for it
	return func(p Plugin, m *discordgo.MessageCreate) bool {
		moduleName := p.Name()
		message := m.Content
		for idx := range commandStrings {
			if singleCommandCheck(moduleName, commandStrings[idx], message) {
				return true
			}
		}
		return false
	}
}

func createPermissionChecker(wantedPerms int) PermissionChecker {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) bool {
		userPerms, _ := s.UserChannelPermissions(m.Author.ID, m.ChannelID)
		return (wantedPerms & userPerms) == wantedPerms
	}
}

func createBotIDChecker() BotIDChecker {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) bool {
		return s.State.User.ID == m.Author.ID
	}
}

// CalculateMinimumPermissions will find the min perms needed to execute any command
func CalculateMinimumPermissions(commands []Command) int {
	perms := math.MaxInt64
	for _, command := range commands {
		perms &= command.PermissionsNeeded()
	}
	return perms
}
