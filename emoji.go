package gofumo

const (
	// EmojiArrowUp is used to signify upwards motion
	EmojiArrowUp = "🔼"
	// EmojiArrowLeft is used to signify leftwards motion
	EmojiArrowLeft = "◀"
	// EmojiArrowRight is used to signify rightwards motion
	EmojiArrowRight = "▶"
	// EmojiCrossMark is used to indicate the failure of an action
	EmojiCrossMark = "❎"
	// EmojiCheckMark is used to indicate the success of an action
	EmojiCheckMark = "✅"

	EmojiRaisedHand = "✋"

	EmojiLightToneSkinModifier = "🏻"
)

var (
	NumberToEmoji = map[int]string{
		0: "0⃣",
		1: "1⃣",
		2: "2⃣",
		3: "3⃣",
		4: "4⃣",
		5: "5⃣",
		6: "6⃣",
		7: "7⃣",
		8: "8⃣",
		9: "9⃣",
	}
	EmojiToNumber = map[string]int{}
)

func init() {

	// we'll need this for checking reacts quickly
	for num, emoji := range NumberToEmoji {
		EmojiToNumber[emoji] = num
	}
}
