package spells

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type BaseSpell struct {
	*mannymon.BaseMove
	Emoji string

	hasBeenUsed bool
}

func (spell *BaseSpell) GetEmoji() string {
	return spell.Emoji
}

func (spell *BaseSpell) IsUsable() bool {
	return !spell.hasBeenUsed
}

func NewSpell(Name, Desc, Emoji, TemplateString string, Nums []int, wrapper func(Nums []int, t mannymon.Template) mannymon.MoveFunc) mannymon.Spell {
	return &BaseSpell{
		BaseMove: mannymon.NewMove(Name, Desc, TemplateString, Nums, wrapper),
		Emoji:    Emoji,
	}
}

// Heal is a simple spell that does exactly that, heal the mon
func NewHeal() mannymon.Spell {
	spell := &BaseSpell{
		BaseMove: &mannymon.BaseMove{
			Name: "Heal",
			Desc: "One-time use to heal %d HP. Immediately ends your turn",
			Nums: []int{3},
		},
		Emoji: "Heal:545060296390082580",
	}
	nums := spell.Nums
	success := mannymon.NewTemplate("{{.self}} cast heal and recovered {{.heal}} health")
	failure := mannymon.NewTemplate("{{self}} cast their spell but it was unusable? How??")
	spell.Func = func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
		if spell.IsUsable() {
			self.Heal(enemy, nums[0], opts)
			spell.hasBeenUsed = true
			return mannymon.TurnDone, success, nil
		}
		return mannymon.TurnDone, failure, nil
	}
	return spell
}

func NewBarrier() mannymon.Spell {
	spell := &BaseSpell{
		BaseMove: &mannymon.BaseMove{
			Name: "Barrier",
			Desc: "One-time use to negate the next turn's attack. Immediately ends your turn. If opponent does not attack, the spell is wasted",
			Nums: []int{},
		},
		Emoji: "Barrier:545060296381825024",
	}
	success := mannymon.NewTemplate("{{.self}} cast barrier and is defended for the next turn")
	failure := mannymon.NewTemplate("{{self}} cast their spell but it was unusable? How??")
	spell.Func = func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
		if !spell.IsUsable() {
			return mannymon.TurnDone, failure, nil
		}

		spell.hasBeenUsed = true
		origTurn := opts.TurnCount
		enemy.AddHook(mannymon.PreAttack, func(_, _ mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
			curTurn := opts.TurnCount
			if origTurn+1 == curTurn && num > 0 {
				return &mannymon.HookData{
					Num:               0,
					Priority:          1,
					TemplateExtension: "{{enemy}} negated the attack!",
				}
			}
			return &mannymon.HookData{
				Num: num,
			}

		})
		return mannymon.TurnDone, success, nil
	}
	return spell
}

func NewCleanse() mannymon.Spell {
	spell := &BaseSpell{
		BaseMove: &mannymon.BaseMove{
			Name: "Cleanse",
			Desc: "Removes negative status effects from yourself and roll again.",
			Nums: []int{},
		},
		Emoji: "Cleanse:561714091446763544",
	}
	success := mannymon.NewTemplate("{{.self}} cast Cleanse and removed all harmful status effects")
	failure := mannymon.NewTemplate("{{self}} cast their spell but it was unusable? How??")
	spell.Func = func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
		if !spell.IsUsable() {
			return mannymon.TurnDone, failure, nil
		}
		spell.hasBeenUsed = true
		self.RemoveStatus(mannymon.StatusCharmed)
		return mannymon.TurnExtra, success, nil

	}
	return spell
}

func NewIgnite() mannymon.Spell {
	spell := &BaseSpell{
		BaseMove: &mannymon.BaseMove{
			Name: "Ignite",
			//Desc: "Deals %d damage per turn for %d turns and reduces healing by %d",
			Desc: "One-time use to deal %d damage and end your turn",
			Nums: []int{2},
		},
		Emoji: "Ignite:561713960852783115",
	}
	success := mannymon.NewTemplate("{{.self}} cast Ignite and burned {{enemy}} for {{damage}} HP")
	failure := mannymon.NewTemplate("{{self}} cast their spell but it was unusable? How??")
	spell.Func = func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
		if !spell.IsUsable() {
			return mannymon.TurnDone, failure, nil
		}
		spell.hasBeenUsed = true
		self.DealDamage(enemy, spell.Nums[0], opts)
		return mannymon.TurnExtra, success, nil

	}
	return spell
}

func init() {
	mannymon.AddSpell("heal", NewHeal)
	mannymon.AddSpell("barrier", NewBarrier)
}
