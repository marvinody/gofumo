package game

import (
	"fmt"
	"math"
	"strings"
	"time"

	"bitbucket.org/marvinody/gofumo"
	"bitbucket.org/marvinody/gofumo/utils"
	"github.com/bwmarrin/discordgo"
)

// Display mons in a big list with pagination
// Can "zoom" into the mon and see details regarding moves and page within that
// Not sure if I'll get hit with rate limits here, but hopefully not
func (mm *mannymon) listHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	pages := int(math.Ceil(float64(len(mons)) / float64(monsPerPage)))
	page := 0
	embed := makeInitialListEmbed(page, pages)
	numChoices := clamp(len(mons), monsPerPage)
	closeUpMonView, closeUpMonIdx := false, 0
	msg, _ := s.ChannelMessageSendEmbed(m.ChannelID, embed.Get())
	if pages > 1 {
		s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowLeft)
		s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowRight)
	}
	for i := 1; i <= numChoices; i++ {
		s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.NumberToEmoji[i])
	}
	utils.WaitUntilReact(s, msg.ID, func(e *discordgo.MessageReactionAdd) bool {
		emoji := e.Emoji.APIName()
		// if arrows, just change the page
		if emoji == gofumo.EmojiArrowLeft || emoji == gofumo.EmojiArrowRight {
			addNum := -1
			if emoji == gofumo.EmojiArrowRight {
				addNum = +1
			}

			if closeUpMonView {
				closeUpMonIdx = utils.Mod(closeUpMonIdx+addNum, len(mons))
			} else {
				page = utils.Mod(page+addNum, pages)
			}
		} else if num, ok := gofumo.EmojiToNumber[emoji]; ok && !closeUpMonView { // if user gave a number, let's get more info
			// if number we're not using, do nothing
			if num == 0 || num > numChoices {
				return false
			}
			closeUpMonView = true
			closeUpMonIdx = page*monsPerPage + num - 1
			closeUpMonIdx = clamp(closeUpMonIdx, len(mons)-1)
			s.MessageReactionsRemoveAll(m.ChannelID, msg.ID)
			s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowLeft)
			// give user an exit route to go back up
			s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowUp)
			s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowRight)

		} else if emoji == gofumo.EmojiArrowUp && closeUpMonView {
			// go back to page view
			closeUpMonView = false
			// remove self reacts
			s.MessageReactionsRemoveAll(m.ChannelID, msg.ID)
			// only add arrows if useful
			if pages > 1 {
				s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowLeft)
				s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowRight)
			}
			// add these back if needed
			for i := 1; i <= numChoices; i++ {
				s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.NumberToEmoji[i])
			}
		} else {
			return false
		}
		// actually do the specific rendering
		if closeUpMonView {
			s.ChannelMessageEditEmbed(m.ChannelID, msg.ID, makeMonEmbed(closeUpMonIdx))
		} else {
			s.ChannelMessageEditEmbed(m.ChannelID, msg.ID, makeInitialListEmbed(page, pages).Get())
		}
		return false
	}, time.Now().Add(timeToViewMons))
}

func makeMonEmbed(idx int) *discordgo.MessageEmbed {
	mon := mons[idx]
	embed := utils.NewEmbed().
		SetTitle(fmt.Sprintf("%s - %d HP", mon.GetName(), mon.GetHP())).
		SetDesc(mon.GetDesc()).
		SetColor(gofumo.EmbedColor).
		SetFooter("", fmt.Sprintf("%d / %d", idx+1, len(mons)))
	for idx, move := range mon.GetMoves() {
		name := move.GetName()
		formattedMoveDesc := move.GetDesc()
		nums := move.GetNums()
		if len(nums) > 0 && strings.Count(formattedMoveDesc, "%d") == len(nums) {
			formattedMoveDesc = fmt.Sprintf(formattedMoveDesc, intToInterface(nums)...)
		}
		embed.AddField(fmt.Sprintf("**%d**: %s", idx, name), formattedMoveDesc, false)
	}
	return embed.Get()
}
