package game

import (
	"fmt"
	"log"
	"strings"

	"bitbucket.org/marvinody/gofumo"
	"github.com/bwmarrin/discordgo"
)

func (mm *mannymon) logHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	fightLog := mm.fightLog.Get(m.Author.ID)
	pmCh, _ := s.UserChannelCreate(m.Author.ID)
	if fightLog == nil {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		s.ChannelMessageSend(pmCh.ID, "I don't seem to have a recent fight in my history for you. \nTry fighting someone and then request again")
		return
	}
	for _, strs := range chunkIntoLimits(fightLog, 1800) {
		combined := strings.Join(strs, "\n")
		_, err := s.ChannelMessageSend(pmCh.ID, fmt.Sprintf("```%s```", combined))
		if err != nil {
			log.Println(err)
		}
	}
	s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)

}

func chunkIntoLimits(strings []string, charLimit int) [][]string {
	output := [][]string{}
	curStringList := []string{}
	curStringTotal := 0

	for _, s := range strings {
		//     string lens     new lines     new line
		if curStringTotal+len(curStringList)+len(s) > charLimit {
			output = append(output, curStringList)
			curStringList = []string{}
			curStringTotal = 0
		}
		curStringList = append(curStringList, s)
		curStringTotal += len(s)
	}
	output = append(output, curStringList)
	return output
}
