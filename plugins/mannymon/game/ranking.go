package game

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"bitbucket.org/marvinody/gofumo"
	"github.com/bwmarrin/discordgo"
)

func (mm *mannymon) monrankingHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	mons, err := mm.LoadTopMons()
	if err != nil || len(mons) == 0 {
		log.Println(err)
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	s.ChannelMessageSend(m.ChannelID, prettyPrintMonRanking(mons))

}

func prettyPrintMonRanking(mons []MonsterRanking) string {

	nameList, fightList, winList, averageCountList := monsterRankingsToLists(mons)
	// get the padding for each one now
	namePadding := longestStringLen(len("User"), nameList)
	fightPadding := longestStringLen(len("Fights"), fightList)
	winPadding := longestStringLen(len("Wins"), winList)
	averageCountPadding := longestStringLen(len("Average Turn Count"), averageCountList)

	out := fmt.Sprintf("```|%*s|%*s|%*s|%*s|\n",
		namePadding, "Monster",
		fightPadding, "Fights",
		winPadding, "Wins",
		averageCountPadding, "Average Turn Count",
	)
	internalSize := namePadding + fightPadding + winPadding + averageCountPadding + 3
	out += fmt.Sprintf("|%s|\n", strings.Repeat("-", internalSize))
	for idx := range mons {
		out += fmt.Sprintf("|%*s|%*s|%*s|%*s|\n",
			namePadding, nameList[idx],
			fightPadding, fightList[idx],
			winPadding, winList[idx],
			averageCountPadding, averageCountList[idx],
		)
	}
	return out + "```"
}

func (mm *mannymon) rankingHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	defaultOrder := OrderByWins
	topRanks, self, err := mm.LoadTopUsers(s, defaultOrder, m.Author.ID)
	if err != nil || len(topRanks) == 0 || self.user == nil {
		log.Println(err)
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	s.ChannelMessageSend(m.ChannelID, prettyPrintRanking(topRanks, self))

}

func prettyPrintRanking(topRanks []UserRanking, self UserRanking) string {
	usernames := make([]string, len(topRanks))
	for idx, rank := range topRanks {
		usernames[idx] = rank.user.Username
	}
	expList, fightList, winList, _ := userRankingsToLists(topRanks)
	selfExp, selfFights, selfWins := strconv.Itoa(self.exp), strconv.Itoa(self.fights), strconv.Itoa(self.wins)
	// get the padding for each one now
	namePadding := longestUserStringLen(len("User"), self.user.Username, usernames)
	expPadding := longestUserStringLen(len("Experience"), selfExp, expList)
	fightPadding := longestUserStringLen(len("Fights"), selfFights, fightList)
	winPadding := longestUserStringLen(len("Wins"), selfWins, winList)

	out := fmt.Sprintf("```|%*s|%*s|%*s|%*s|\n",
		namePadding, "User",
		expPadding, "Experience",
		fightPadding, "Fights",
		winPadding, "Wins",
	)
	internalSize := namePadding + expPadding + fightPadding + winPadding + 3
	out += fmt.Sprintf("|%s|\n", strings.Repeat("-", internalSize))
	for idx := range topRanks {
		out += fmt.Sprintf("|%*s|%*s|%*s|%*s|\n",
			namePadding, usernames[idx],
			expPadding, expList[idx],
			fightPadding, fightList[idx],
			winPadding, winList[idx],
		)
	}
	out += fmt.Sprintf("|%s|\n", strings.Repeat("-", internalSize))
	out += fmt.Sprintf("|%*s|%*s|%*s|%*s|\n```",
		namePadding, self.user.Username,
		expPadding, selfExp,
		fightPadding, selfFights,
		winPadding, selfWins,
	)

	return out

}

func userRankingsToLists(list []UserRanking) (expList, fightList, winList, lossList []string) {
	expList = make([]string, len(list))
	fightList = make([]string, len(list))
	winList = make([]string, len(list))
	lossList = make([]string, len(list))
	for idx, ranking := range list {
		expList[idx] = strconv.Itoa(ranking.exp)
		fightList[idx] = strconv.Itoa(ranking.fights)
		winList[idx] = strconv.Itoa(ranking.wins)
		lossList[idx] = strconv.Itoa(ranking.losses)
	}
	return
}
func monsterRankingsToLists(list []MonsterRanking) (nameList, fightList, winList, averageCountList []string) {
	nameList = make([]string, len(list))
	fightList = make([]string, len(list))
	winList = make([]string, len(list))
	averageCountList = make([]string, len(list))
	for idx, mon := range list {
		nameList[idx] = mon.name
		fightList[idx] = strconv.Itoa(mon.fights)
		winList[idx] = strconv.Itoa(mon.wins)
		averageCountList[idx] = strconv.Itoa(mon.averageTurnCount)
	}
	return
}

func longestUserStringLen(startLen int, self string, list []string) int {
	max := startLen
	if len(self) > max {
		max = len(self)
	}
	return longestStringLen(max, list)
}
