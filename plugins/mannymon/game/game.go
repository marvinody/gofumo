package game

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"sort"
	"strings"
	"time"

	"bitbucket.org/marvinody/gofumo"
	mon "bitbucket.org/marvinody/gofumo/plugins/mannymon"

	// monsters that we're gonna load up
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/cerberos"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/cujo"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/devilbat"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/geomon"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/muscles"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/ratking"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/rogue"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/succubus"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/mons/zenturtle"

	// load up all spells
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/spells"

	"bitbucket.org/marvinody/gofumo/utils"
	"github.com/bwmarrin/discordgo"
)

var (
	mons         = map[int]mon.Monster{}
	spells       = map[string]mon.Spell{}
	monIntToName = map[int]string{}
	monsPerPage  = 5

	validSpellEmoji = map[string]string{}

	timeToAcceptChallenge = time.Minute * 1
	timeToSelectMons      = time.Minute * 2
	timeToFightMons       = time.Minute * 15
	timeToViewMons        = time.Minute * 3
	timeToSelectSpell     = time.Minute * 2

	BaseMonsterExp = 200
	MaxLevel       = 100

	expectedRollMeanForMoveCount = map[int]float64{
		1: 0,
		2: 0.5,
		3: 0.75,
		4: 0.875,
		5: 0.933,
		6: 0.969,
		7: 0.985,
		8: 0.995,
		9: 0.999,
	}
)

const (
	prefix            = "$"
	userInLobby       = "lobby"
	userInMonSelect   = "select"
	userInSpellSelect = "spell"
	userInFight       = "fight"
)

type mannymon struct {
	imgurClientID string

	lock     *utils.LockMapString
	fightLog *utils.LockMapArrString

	db *sql.DB
}

func (mm *mannymon) Name() string {
	return "mannymon"
}

func (mm *mannymon) Description() string {
	return "Do battle with Mannymons"
}

func (mm *mannymon) Help(userPerms int) *discordgo.MessageEmbed {
	return gofumo.MakeEmbedForPlugin(mm, userPerms)
}

func (mm *mannymon) MinimumPermissions() int {
	return gofumo.CalculateMinimumPermissions(mm.Commands())
}

func (mm *mannymon) Commands() []gofumo.Command {
	return []gofumo.Command{
		gofumo.NewMultiSubCommand(
			[]gofumo.CommandSyntax{
				gofumo.CommandSyntax{"mannymon", "help", "Displays this page"},
				gofumo.CommandSyntax{"mannymon", "duel @<user>", "Will attempt to start a game with requested user"},
				gofumo.CommandSyntax{"mannymon", "dex", "List out all the mons available"},
				gofumo.CommandSyntax{"mannymon", "log", "PMs you your most recent full fight log"},
				gofumo.CommandSyntax{"mannymon", "rankings", "Display player rankings sorted by wins"},
				gofumo.CommandSyntax{"mannymon", "monrankings", "Display monster rankings sorted by wins"},
			}, prefix, false, discordgo.PermissionSendMessages, mm.MannymonHandler()),
	}
}

func (mm *mannymon) MannymonHandler() gofumo.EventHandlerGenerator {
	return func(botChecker gofumo.BotIDChecker, comChecker gofumo.CommandChecker, permChecker gofumo.PermissionChecker) gofumo.EventHandler {
		return func(s *discordgo.Session, m *discordgo.MessageCreate) {
			defer func() {
				/*if r := recover(); r != nil {
					log.Println("Recovered in mannymon:", r, m.Content)
				}*/
			}()
			if botChecker(s, m) {
				return
			}
			if !comChecker(mm, m) {
				return
			}
			if !permChecker(s, m) {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			// not the bot, correct base command, and correct perms are known
			split := strings.Split(m.Content, " ")
			if len(split) < 2 {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			command := split[1]
			if command == "help" {
				mm.helpHandler(s, m)
			} else if command == "duel" {
				mm.startHandler(s, m)
			} else if command == "dex" {
				mm.listHandler(s, m)
			} else if command == "rankings" {
				mm.rankingHandler(s, m)
			} else if command == "monrankings" {
				mm.monrankingHandler(s, m)
			} else if command == "log" {
				mm.logHandler(s, m)
			} else {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
			}

		}
	}
}

func (mm *mannymon) helpHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	// pull perms
	userPerms, err := s.UserChannelPermissions(m.Author.ID, m.ChannelID)
	if err != nil {
		log.Println(err)
		return
	}
	minPluginPerms := mm.MinimumPermissions()
	matchingPerms := userPerms & minPluginPerms
	// if user doesn't have min perms needed to use any command, don't bother
	if minPluginPerms != matchingPerms {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	// gen help page for perms given
	help := mm.Help(userPerms)
	s.ChannelMessageSendEmbed(m.ChannelID, help)
}

func longestStringLen(startLen int, list []string) int {
	max := startLen
	for _, w := range list {
		if len(w) > max {
			max = len(w)
		}
	}
	return max
}

func intToInterface(l []int) []interface{} {
	new := make([]interface{}, len(l))
	for idx, val := range l {
		new[idx] = val
	}
	return new
}

func expToLevel(exp int) int {
	// Don't really like this way of doing it, but if the exp function doesn't
	// have an easy inverse, then this is the best way?
	// Makes it dependent on 1 thing only
	// maybe cache the values into map somewhere and do it for low levels?
	for n := 1; n < MaxLevel; n++ {
		if exp < levelToExp(n) {
			return n - 1
		}
	}
	return MaxLevel
}

func levelToExp(level int) int {
	return int(4.0 / 5.0 * math.Pow(float64(level), 3))
}

type QueryOrder string

const (
	OrderByExp    QueryOrder = "exp"
	OrderByFights QueryOrder = "fights"
	OrderByWins   QueryOrder = "wins"
	OrderByLosses QueryOrder = "losses"
)

type UserRanking struct {
	user   *discordgo.User
	exp    int
	fights int
	wins   int
	losses int
}

// LoadTopUsers will pull an array of the top (currently) 20 users ordered by QueryOrder
// and the 2nd return is the user itself. The user may or may not be in the top 20
func (mm *mannymon) LoadTopUsers(s *discordgo.Session, order QueryOrder, userID string) ([]UserRanking, UserRanking, error) {
	query1 := `SELECT id, exp, fights, wins, losses FROM mannymon_rankings ORDER BY ? DESC LIMIT 20`
	query2 := `SELECT id, exp, fights, wins, losses FROM mannymon_rankings WHERE id LIKE ?`
	stmt1, err := mm.db.Prepare(query1)
	if err != nil {
		return nil, UserRanking{}, err
	}
	defer stmt1.Close()
	results, err := stmt1.Query(order)
	if err != nil {
		return nil, UserRanking{}, err
	}
	defer results.Close()
	var (
		id           string
		exp, fights  int
		wins, losses int
	)
	// grab all the top 20 users
	rankings := make([]UserRanking, 0, 20)
	for results.Next() {
		results.Scan(&id, &exp, &fights, &wins, &losses)
		user, err := s.User(id)
		if err != nil {
			return nil, UserRanking{}, err
		}
		rankings = append(rankings, UserRanking{
			user, exp, fights, wins, losses,
		})
	}
	results.Close()
	// and now let's grab the user who requested it
	stmt2, err := mm.db.Prepare(query2)
	if err != nil {
		return nil, UserRanking{}, err
	}
	defer stmt2.Close()
	results, err = stmt2.Query(userID)
	if err != nil {
		return nil, UserRanking{}, err
	}
	var selfRank UserRanking
	if results.Next() {
		results.Scan(&id, &exp, &fights, &wins, &losses)
		user, err := s.User(userID)
		if err != nil {
			return nil, UserRanking{}, err
		}
		selfRank = UserRanking{
			user, exp, fights, wins, losses,
		}
	}
	results.Close()
	return rankings, selfRank, nil
}

type MonsterRanking struct {
	name             string
	fights           int
	wins             int
	averageTurnCount int
}

func (mm *mannymon) LoadTopMons() ([]MonsterRanking, error) {
	mons := make([]MonsterRanking, 0, len(mon.Mons))
	query := `SELECT name, SUM(1) AS fights, SUM(won) AS wins, AVG(turnLength) AS avgTurns FROM mannymon_monsterstats GROUP BY name`
	results, err := mm.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer results.Close()
	for results.Next() {
		var (
			name     string
			fights   int
			wins     int
			avgTurns float64
		)
		results.Scan(&name, &fights, &wins, &avgTurns)
		mons = append(mons, MonsterRanking{
			name, fights, wins, int(avgTurns),
		})
	}
	results.Close()
	sort.Slice(mons, func(i int, j int) bool {
		return mons[i].wins > mons[j].wins
	})
	return mons, nil
}

func (mm *mannymon) SaveBattleOutcome(players []*discordgo.User, mons []mon.Monster, turnCount, winner int) {
	stmt, err := mm.db.Prepare(`
		INSERT INTO mannymon_rankings (id, fights, wins, losses) VALUES (?, ?, ?, ?)
		ON CONFLICT(id) DO UPDATE SET fights=fights+1, wins=wins+excluded.wins, losses=losses+excluded.losses
		`)
	defer stmt.Close()
	if err != nil {
		log.Println(err)
		return
	}
	for idx, player := range players {
		wins, losses := 0, 0
		if winner == idx {
			wins = 1
		} else {
			losses = 1
		}
		stmt.Exec(player.ID, 1, wins, losses)
	}
	stmt.Close()
	stmt, err = mm.db.Prepare(`
		INSERT INTO mannymon_monsterstats (name, turnLength, won) VALUES (?, ?, ?)`)
	if err != nil {
		log.Println(err)
		return
	}
	for idx, mon := range mons {
		won := false
		if idx == winner {
			won = true
		}
		stmt.Exec(mon.GetName(), turnCount, won)
	}
	stmt.Close()
}

func (mm *mannymon) LoadUserExp(userID string) int {
	query := `SELECT exp FROM mannymon_rankings WHERE id LIKE ?`
	rows, _ := mm.db.Query(query, userID)
	exp := 1
	if rows.Next() { // if cause only 1 result
		rows.Scan(&exp)
	}
	rows.Close()
	return exp
}

func (mm *mannymon) SaveUserExp(userID string, expIncrease int) {
	query := `INSERT INTO mannymon_rankings(id, exp) VALUES (?, ?)
	ON CONFLICT(id) DO UPDATE SET exp=exp+?;
	`
	// +1 to ensure no 0 exp somehow
	_, err := mm.db.Exec(query, userID, expIncrease+1, expIncrease)
	if err != nil {
		log.Println(err)
	}
}

func makeInitialListEmbed(page, pages int) *utils.Embed {
	embed := utils.NewEmbed().
		SetTitle("MannyDex").
		SetColor(gofumo.EmbedColor).
		SetDesc("React with number to get more detailed info!")
	startIdx := page * monsPerPage
	endIdx := startIdx + monsPerPage
	endIdx = clamp(endIdx, len(mons))
	for i := startIdx; i < endIdx; i++ {
		mon := mons[i]
		embed.AddField(
			fmt.Sprintf("%d: **%s** - *%s*", i-startIdx+1, mon.GetName(), mon.GetEpithet()),
			fmt.Sprintf("＀＀%s", mon.GetDesc()), false)
	}
	embed.SetFooter("", fmt.Sprintf("%d / %d", page+1, pages))
	return embed
}

func makeInitialSpellEmbed() *utils.Embed {
	embed := utils.NewEmbed().
		SetTitle("Spell List").
		SetColor(gofumo.EmbedColor).
		SetDesc("React with number to get more detailed info!")

	for _, spell := range spells {
		embed.AddField(
			fmt.Sprintf("**%s** - %s", spell.GetName(), spell.GetEmoji()),
			fmt.Sprintf("＀＀%s", spell.GetDesc()), false)
	}
	return embed
}

func (mm *mannymon) Load(db *sql.DB, keys map[string]string) {
	mm.imgurClientID = keys["imgur"]
	mm.db = db
	stmt := `
	CREATE TABLE IF NOT EXISTS mannymon_rankings (
		id TEXT PRIMARY KEY UNIQUE,
		exp INT DEFAULT 1,
		fights INT DEFAULT 0,
		wins INT DEFAULT 0,
		losses INT DEFAULT 0);

	CREATE TABLE IF NOT EXISTS mannymon_monsterstats (
		id INT PRIMARY KEY UNIQUE,
		name TEXT,
		turnLength INT,
		won BOOL
	);
	CREATE INDEX IF NOT EXISTS nameIndex ON mannymon_monsterstats ( name );
	`
	_, err := db.Exec(stmt)
	if err != nil {
		log.Fatal(err)
	}
	for name := range mon.Mons {
		log.Printf("\tLoaded %s\n", name)
	}
}

func (mm *mannymon) Save(db *sql.DB) {

}

func clamp(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func init() {
	blankMon := mon.New("", "", "", 0)
	sortedMons := make([]string, 0, len(mon.Mons))
	for name := range mon.Mons {
		sortedMons = append(sortedMons, name)
	}
	sort.Strings(sortedMons)
	for idx, name := range sortedMons {
		constructor := mon.Mons[name]
		mons[idx] = constructor()
		mons[idx].Init(blankMon, nil)
		monIntToName[idx] = name
		idx++
	}

	for name, spellCon := range mon.Spells {
		spell := spellCon()
		spells[name] = spell
		validSpellEmoji[spell.GetEmoji()] = name
	}

	gofumo.RegisterPlugin(&mannymon{
		lock:     utils.NewLockMap(),
		fightLog: utils.NewLockMapArrString(),
	})
}
