package game

import (
	"container/ring"
	"fmt"
	"hash/crc32"
	"image"
	"log"
	"math"
	"math/rand"
	"strings"
	"time"

	"bitbucket.org/marvinody/gofumo"
	mon "bitbucket.org/marvinody/gofumo/plugins/mannymon"
	"bitbucket.org/marvinody/gofumo/utils"
	"github.com/bwmarrin/discordgo"
)

func (mm *mannymon) startHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	players := []*discordgo.User{m.Author}
	// no mention, can't challenge
	if len(m.Mentions) != 1 {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	// can't challenge self
	if m.Author.ID == m.Mentions[0].ID {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	// can't challenge someone while in battle yourself
	// so if it's not empty it means that we're in something already
	if !mm.lock.Check(m.Author.ID, "") {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	// can't challenge someone while they're in a battle or similar
	if !mm.lock.Check(m.Mentions[0].ID, "") {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
		return
	}
	// set both users into lobby
	mm.lock.Set(m.Author.ID, userInLobby)
	mm.lock.Set(m.Mentions[0].ID, userInLobby)

	wantedPlayer := m.Mentions[0]
	acceptEmote := gofumo.EmojiCheckMark
	rejectEmote := gofumo.EmojiCrossMark
	challengeMsg, err := s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("%s! %s has challenged you to a duel! React with %s to accept, %s to deny (auto-reject in %s)", wantedPlayer.Mention(), m.Author.Username, acceptEmote, rejectEmote, timeToAcceptChallenge.String()))
	if err != nil {
		log.Println(err)
		return
	}
	// then wait for 2nd player
	s.MessageReactionAdd(m.ChannelID, challengeMsg.ID, gofumo.EmojiCheckMark)
	s.MessageReactionAdd(m.ChannelID, challengeMsg.ID, gofumo.EmojiCrossMark)
	utils.WaitUntilReact(s, challengeMsg.ID, func(emote *discordgo.MessageReactionAdd) bool {
		if len(players) == 2 {
			return true
		}
		if wantedPlayer.ID != emote.UserID {
			return false
		}
		switch emote.Emoji.APIName() {
		case rejectEmote:
			mm.lock.Unset(m.Author.ID)
			mm.lock.Unset(m.Mentions[0].ID)
			s.ChannelMessageEdit(m.ChannelID, challengeMsg.ID, fmt.Sprintf("%s! %s has challenged you to a duel! Duel rejected...", wantedPlayer.Mention(), m.Author.Username))
			return true
		case acceptEmote:
			wantedUser, _ := s.User(emote.UserID)
			players = append(players, wantedUser)
			// use defer here to remove the handler and run the code
			// not 100% sure this works but maybe
			defer mm.chooseMonHandler(s, m.ChannelID, players)
			return true
		default:
			return false
		}
		// else it's gonna be accept
	}, time.Now().Add(timeToAcceptChallenge))

	// unset only if user still in lobby after timeout
	utils.DoAfter(timeToAcceptChallenge, func() {
		if mm.lock.Check(m.Author.ID, userInLobby) {
			mm.lock.Unset(m.Author.ID)
			// some feedback to the user that timeout happened
			s.ChannelMessageEdit(m.ChannelID, challengeMsg.ID, fmt.Sprintf("%s! %s has challenged you to a duel! Duel timed out...", wantedPlayer.Mention(), m.Author.Username))
		}
		if mm.lock.Check(m.Mentions[0].ID, userInLobby) {
			mm.lock.Unset(m.Mentions[0].ID)
		}
	})
}

func (mm *mannymon) chooseMonHandler(s *discordgo.Session, channelID string, players []*discordgo.User) {
	pages := int(math.Ceil(float64(len(mons)) / float64(monsPerPage)))
	numChoices := clamp(len(mons), monsPerPage)
	monChoices := []int{}
	// TODO: Pagination
	// not gonna do pagination here yet because that'd be annoying
	// it should be done once more mons come into play tho
	page := 0
	embed := makeInitialListEmbed(page, pages)

	waitingOnPlayer := players[0]
	embed.SetDesc("") // remove the default desc.
	embed.SetAuthor(fmt.Sprintf("%s, select your mon!", waitingOnPlayer.Username), waitingOnPlayer.AvatarURL(""))
	m, err := s.ChannelMessageSendEmbed(channelID, embed.Get())
	if err != nil {
		log.Println(err)
		return
	}
	if pages > 1 {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiArrowLeft)
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiArrowRight)
	}
	for i := 1; i <= numChoices; i++ {
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.NumberToEmoji[i])
	}
	// prevent users from getting started in another fight
	mm.lock.Set(players[0].ID, userInMonSelect)
	mm.lock.Set(players[1].ID, userInMonSelect)

	utils.WaitUntilReact(s, m.ID, func(e *discordgo.MessageReactionAdd) bool {
		if e.UserID != waitingOnPlayer.ID {
			return false
		}
		emoji := e.Emoji.APIName()
		if emoji == gofumo.EmojiArrowLeft || emoji == gofumo.EmojiArrowRight {

			addNum := -1
			if emoji == gofumo.EmojiArrowRight {
				addNum = +1
			}
			page = utils.Mod(page+addNum, pages)
			embed = makeInitialListEmbed(page, pages)
			embed.SetDesc("") // remove the default desc.
			embed.SetAuthor(fmt.Sprintf("%s, select your mon!", waitingOnPlayer.Username), waitingOnPlayer.AvatarURL(""))
			s.ChannelMessageEditEmbed(channelID, m.ID, embed.Get())
			return false

		} else if num, ok := gofumo.EmojiToNumber[emoji]; ok { // if user gave a number, let's get more info
			// if number we're not using, do nothing
			if num == 0 || num > numChoices {
				return false
			}
			// on the last page, only some numbers may be valid
			// so only if on the last page, and uneven mon count
			if page == pages-1 && len(mons)%monsPerPage != 0 {
				highestNum := (len(mons) % monsPerPage)
				if num > highestNum {
					return false
				}
			}
			// correct player and valid choice now
			monChoice := page*monsPerPage + num - 1
			monChoices = append(monChoices, monChoice)
			// we're done
			if waitingOnPlayer.ID == players[1].ID && len(monChoices) == 2 {
				defer mm.selectSpellHandler(s, m, players, monChoices)
				return true
			}
			// else, we just got p1, now get p2
			waitingOnPlayer = players[1]
			embed.SetDesc("") // remove the default desc.
			embed.SetAuthor(fmt.Sprintf("%s, select your mon!", waitingOnPlayer.Username), waitingOnPlayer.AvatarURL(""))
			s.ChannelMessageEditEmbed(m.ChannelID, m.ID, embed.Get())
			return false
		}
		return false
	}, time.Now().Add(timeToSelectMons))

	utils.DoAfter(timeToSelectMons, func() {
		// if still in monselect after timeout, pull them both out
		if mm.lock.Check(players[0].ID, userInMonSelect) {
			mm.lock.Unset(players[0].ID)
		}
		if mm.lock.Check(players[1].ID, userInMonSelect) {
			mm.lock.Unset(players[1].ID)
		}
	})
}

func (mm *mannymon) selectSpellHandler(s *discordgo.Session, m *discordgo.Message, players []*discordgo.User, monChoices []int) {
	mm.lock.Set(players[0].ID, userInSpellSelect)
	mm.lock.Set(players[1].ID, userInSpellSelect)
	channelID := m.ChannelID
	waitingOnPlayer := players[0]
	embed := makeInitialSpellEmbed()

	spellChoices := []string{}

	embed.SetDesc("") // remove the default desc.
	embed.SetAuthor(fmt.Sprintf("%s, select your spell!", waitingOnPlayer.Username), waitingOnPlayer.AvatarURL(""))
	m, err := s.ChannelMessageSendEmbed(channelID, embed.Get())
	if err != nil {
		log.Println(err)
		return
	}

	utils.WaitUntilReact(s, m.ID, func(e *discordgo.MessageReactionAdd) bool {
		if e.UserID != waitingOnPlayer.ID {
			return false
		}
		emoji := e.Emoji.APIName()
		if spellName, ok := validSpellEmoji[emoji]; ok { // if valid emoji, then we good

			spellChoices = append(spellChoices, spellName)

			// we're done
			if waitingOnPlayer.ID == players[1].ID && len(monChoices) == 2 {
				defer mm.fightMonHandler(s, m, players, monChoices, spellChoices)
				return true
			}
			// else, we just got p1, now get p2
			waitingOnPlayer = players[1]
			embed.SetAuthor(fmt.Sprintf("%s, select your spell!", waitingOnPlayer.Username), waitingOnPlayer.AvatarURL(""))
			s.ChannelMessageEditEmbed(m.ChannelID, m.ID, embed.Get())
			return false
		}
		return false
	}, time.Now().Add(timeToSelectMons))

	utils.DoAfter(timeToSelectMons, func() {
		// if still in spellselect after timeout, pull them both out
		if mm.lock.Check(players[0].ID, userInSpellSelect) {
			mm.lock.Unset(players[0].ID)
		}
		if mm.lock.Check(players[1].ID, userInSpellSelect) {
			mm.lock.Unset(players[1].ID)
		}
	})

}

func (mm *mannymon) fightMonHandler(s *discordgo.Session, m *discordgo.Message, players []*discordgo.User, monChoices []int, spellChoices []string) {
	animationsEnabled := false

	mm.lock.Set(players[0].ID, userInFight)
	mm.lock.Set(players[1].ID, userInFight)

	// "constructors"
	playerMons := []mon.Monster{
		mon.Mons[monIntToName[monChoices[0]]](),
		mon.Mons[monIntToName[monChoices[1]]](),
	}
	playerSpells := []mon.Spell{
		mon.Spells[spellChoices[0]](),
		mon.Spells[spellChoices[1]](),
	}
	p1 := playerMons[0]
	p2 := playerMons[1]
	// option gen
	options := &mon.Options{
		RNG:       rand.New(rand.NewSource(int64(time.Now().Nanosecond()))),
		TurnCount: 0,
	}
	// initialize them and let them build any hooks needed
	p1.Init(p2, options)
	p2.Init(p1, options)

	monMoveset := [][]mon.Move{
		p1.GetMoves(),
		p2.GetMoves(),
	}

	playerRollHistory := [][]int{
		[]int{},
		[]int{},
	}

	// exp stuff
	p1Exp := mm.LoadUserExp(players[0].ID)
	p2Exp := mm.LoadUserExp(players[1].ID)
	p1Level, p2Level := expToLevel(p1Exp), expToLevel(p2Exp)
	levels := []int{p1Level, p2Level}

	fullMoveLog := []string{}
	// ring to overwrite prev stuff instead of list
	turnLog := ring.New(3)
	currentTurn := 0
	var embed *utils.Embed

	nextIdx := func() int {
		return utils.Mod(currentTurn+1, 2)
	}
	/*prevIdx := func() int {
		return utils.Mod(currentTurn-1, 2)
	}*/
	// roll here so we can embed info, emote after
	roll := playerMons[currentTurn].Roll(options)
	// initial embed creations
	if animationsEnabled {
		makeFightImage(players, playerMons, currentTurn)
	} else {
		embed = makeFightEmbedNoImage(players, playerMons, levels, monMoveset, playerSpells, currentTurn, nextIdx(), options.TurnCount, roll, turnLog, false)
	}

	msg, err := s.ChannelMessageEditEmbed(m.ChannelID, m.ID, embed.Get())
	if err != nil {
		log.Println(err)
		return
	}
	// setup reacts
	reactWithRoll(s, m.ChannelID, m.ID, roll)
	if playerSpells[currentTurn].IsUsable() {

		s.MessageReactionAdd(m.ChannelID, m.ID, playerSpells[currentTurn].GetEmoji())
	}
	// now the "main loop"
	utils.WaitUntilReact(s, msg.ID, func(e *discordgo.MessageReactionAdd) bool {
		if e.UserID != players[currentTurn].ID {
			return false
		}
		emoji := e.Emoji.APIName()
		spellEmoji := ""
		if playerSpells[currentTurn].IsUsable() {
			spellEmoji = playerSpells[currentTurn].GetEmoji()
		}
		if _, ok := gofumo.EmojiToNumber[emoji]; !ok && spellEmoji != emoji {
			return false
		}
		num := gofumo.EmojiToNumber[emoji]
		if num > roll && spellEmoji != emoji {
			return false
		}
		// so we have valid user and valid roll now
		// update options for anything needing it
		options.IsSpell = spellEmoji == emoji
		options.MoveRolled = roll
		options.MovePicked = num
		// log hps to see how much damage it does, and grab status
		playerRollHistory[currentTurn] = append(playerRollHistory[currentTurn], roll)
		selfHPBefore := playerMons[currentTurn].GetHP()
		enemyHPBefore := playerMons[nextIdx()].GetHP()
		var (
			status   mon.TurnStatus
			template mon.Template
			data     mon.MoveData
		)
		if spellEmoji == emoji {
			status, template, data = playerSpells[currentTurn].GetFunc()(playerMons[currentTurn], playerMons[nextIdx()], options)
		} else {
			status, template, data = playerMons[currentTurn].DoMove(num, playerMons[nextIdx()], options)
		}
		selfHPAfter := playerMons[currentTurn].GetHP()
		enemyHPAfter := playerMons[nextIdx()].GetHP()
		if data == nil {
			data = map[string]interface{}{}
		}
		// x, enemyX is from current turn's persepective.
		// so damage is how much damage done to enemy
		// enemyDamage is how much damage enemy done to self
		// yes, hp and damage are negated but leaving them full is clearer, I think
		data["damage"] = enemyHPBefore - enemyHPAfter
		data["enemyDamage"] = selfHPBefore - selfHPAfter
		data["heal"] = selfHPAfter - selfHPBefore
		data["enemyHeal"] = enemyHPAfter - enemyHPBefore
		data["self"] = fmt.Sprintf("%s (%s)", playerMons[currentTurn].GetName(), players[currentTurn].Username)
		data["enemy"] = fmt.Sprintf("%s (%s)", playerMons[nextIdx()].GetName(), players[nextIdx()].Username)
		data["nums"] = monMoveset[currentTurn][num].GetNums()

		// set cur node and move to next
		//log.Value = fmt.Sprintf("%s (%s) hit for %d HP using %s (#%d)", playerMons[currentTurn].GetName(), players[currentTurn].Username, enemyHPBefore-enemyHPAfter, monMoveset[currentTurn][num].GetName(), num)
		turnLog.Value = template.Execute(data)
		// log for later
		fullMoveLog = append(fullMoveLog, fmt.Sprintf("%d: %s", options.TurnCount, turnLog.Value.(string)))
		turnLog = turnLog.Next()
		// we got a winner
		if playerMons[nextIdx()].GetHP() <= 0 {
			embed = makeFightEmbedNoImage(players, playerMons, levels, monMoveset, playerSpells, currentTurn, nextIdx(), options.TurnCount, roll, turnLog, true)
			embed.ResetFields()
			embed.SetFooter("", fmt.Sprintf("%d turns", options.TurnCount))

			// some stats
			embed.AddField(fmt.Sprintf("%s roll avg:", players[0].Username), fmt.Sprintf("%3.2f (exp: %3.2f)", calculateMean(playerRollHistory[0]), expectedRollMeanForMoveCount[len(monMoveset[0])]), true)
			embed.AddField(fmt.Sprintf("%s roll avg:", players[1].Username), fmt.Sprintf("%3.2f (exp: %3.2f)", calculateMean(playerRollHistory[1]), expectedRollMeanForMoveCount[len(monMoveset[1])]), true)

			// level up notifs
			p1ExpIncrease, p2ExpIncrease := getExpForBattle(options.TurnCount, p1Level, p2Level, currentTurn)
			p1ExpSum := p1Exp + p1ExpIncrease
			p2ExpSum := p2Exp + p2ExpIncrease
			embed.AddField(fmt.Sprintf("%s exp:", players[0].Username), fmt.Sprintf("%d (+%d) -> %d", p1Exp, p1ExpIncrease, p1ExpSum), true)
			embed.AddField(fmt.Sprintf("%s exp:", players[1].Username), fmt.Sprintf("%d (+%d) -> %d", p2Exp, p2ExpIncrease, p2ExpSum), true)

			p1NewLevel := expToLevel(p1ExpSum)
			if p1NewLevel != p1Level {
				embed.AddField(fmt.Sprintf("%s leveled up!", players[0].Username), fmt.Sprintf("%d -> %d", p1Level, p1NewLevel), false)
			}
			p2NewLevel := expToLevel(p2ExpSum)
			if p2NewLevel != p2Level {
				embed.AddField(fmt.Sprintf("%s leveled up!", players[1].Username), fmt.Sprintf("%d -> %d", p2Level, p2NewLevel), false)
			}
			// actually save exp
			mm.SaveUserExp(players[0].ID, p1ExpIncrease)
			mm.SaveUserExp(players[1].ID, p2ExpIncrease)
			// save win stats
			mm.SaveBattleOutcome(players, playerMons, options.TurnCount, currentTurn)
			// winner notif

			// put this at the end to make it stand out
			embed.AddField("Battle Over", fmt.Sprintf("%s has won", players[currentTurn].Username), false)
			s.ChannelMessageEditEmbed(m.ChannelID, m.ID, embed.Get())
			// if they request their log, send it
			mm.fightLog.Set(players[0].ID, fullMoveLog)
			mm.fightLog.Set(players[1].ID, fullMoveLog)

			mm.lock.Unset(players[0].ID)
			mm.lock.Unset(players[1].ID)
			return true
		}
		// if we need to give extra turn, we just don't change turn idx
		if status != mon.TurnExtra {
			currentTurn = nextIdx()
		} else {
			temp := turnLog.Prev()
			oldStr := temp.Value.(string)
			oldStr += "\nExtra Turn!"
			temp.Value = oldStr
		}
		// turncount always goes up, and then we roll for next turn
		options.TurnCount++
		roll = playerMons[currentTurn].Roll(options)
		if animationsEnabled {
			makeFightImage(players, playerMons, currentTurn)
		} else {
			embed = makeFightEmbedNoImage(players, playerMons, levels, monMoveset, playerSpells, currentTurn, nextIdx(), options.TurnCount, roll, turnLog, false)
		}
		// update with new player info and remove all reacts
		_, err = s.ChannelMessageEditEmbed(m.ChannelID, m.ID, embed.Get())
		if err != nil {
			log.Println(err)
			return true
		}
		reactWithRoll(s, m.ChannelID, m.ID, roll)
		if spell, ok := spells[players[currentTurn].ID]; ok {
			s.MessageReactionAdd(m.ChannelID, m.ID, spell.GetEmoji())
		}
		return false
	}, time.Now().Add(timeToFightMons))

	utils.DoAfter(timeToFightMons, func() {
		// if still in monselect after timeout, pull them both out
		if mm.lock.Check(players[0].ID, userInFight) {
			mm.lock.Unset(players[0].ID)
		}
		if mm.lock.Check(players[1].ID, userInFight) {
			mm.lock.Unset(players[1].ID)
		}
	})

}

func getStacksForMon(monster mon.Monster) string {
	stacks := []string{}
	for _, counter := range monster.GetCounters() {
		if counter.Get() == 0 {
			continue
		}
		stacks = append(stacks, fmt.Sprintf("＀＀%s: %d\n", counter.Name(), counter.Get()))
	}
	return strings.Join(stacks, "\n")

}

func calculateMean(l []int) float64 {
	n := 0
	for _, val := range l {
		n += val
	}
	return float64(n) / float64(len(l))
}

func reactWithRoll(s *discordgo.Session, chanID, msgID string, roll int) {
	s.MessageReactionsRemoveAll(chanID, msgID)
	for i := 0; i <= roll; i++ {
		s.MessageReactionAdd(chanID, msgID, gofumo.NumberToEmoji[i])
	}
}

func makeFightEmbedNoImage(players []*discordgo.User, playerMons []mon.Monster, levels []int, monMoveset [][]mon.Move, playerSpells []mon.Spell, currentTurn, nextTurn, turnCount, roll int, log *ring.Ring, fightOver bool) *utils.Embed {
	// take checksum of their cur player, and we just get the the last 3 bytes
	color := int(crc32.ChecksumIEEE([]byte(players[currentTurn].Username)) & 0xFFFFFF)
	embed := utils.NewEmbed().
		SetColor(color).
		SetAuthor(fmt.Sprintf("%s  (Level %d) - Turn %d", players[currentTurn].Username, levels[currentTurn], turnCount+1), players[currentTurn].AvatarURL(""))
	if fightOver {
		embed.SetAuthor(fmt.Sprintf("%s  (Level %d) won!", players[currentTurn].Username, levels[currentTurn]), players[currentTurn].AvatarURL(""))
	}
	desc := fmt.Sprintf("%s (%s) - **%d HP**\n%s (%s) - **%d HP**\n\n",
		playerMons[currentTurn].GetName(), players[currentTurn].Username, playerMons[currentTurn].GetHP(),
		playerMons[nextTurn].GetName(), players[nextTurn].Username, playerMons[nextTurn].GetHP())
	// add each move and desc to the help if fight ongoing
	if !fightOver {
		for idx, move := range monMoveset[currentTurn] {
			formattedMoveDesc := move.GetDesc()
			nums := move.GetNums()
			if len(nums) > 0 && strings.Count(formattedMoveDesc, "%d") == len(nums) {
				formattedMoveDesc = fmt.Sprintf(formattedMoveDesc, intToInterface(nums)...)
			}
			desc += fmt.Sprintf("**%d**: %s - %s\n", idx, move.GetName(), formattedMoveDesc)
		}
		spell := playerSpells[currentTurn]
		if spell.IsUsable() {
			formattedSpellDesc := spell.GetDesc()
			nums := spell.GetNums()
			if len(nums) > 0 && strings.Count(formattedSpellDesc, "%d") == len(nums) {
				formattedSpellDesc = fmt.Sprintf(formattedSpellDesc, intToInterface(nums)...)
			}
			desc += fmt.Sprintf("**%s** <:%s> - %s\n", spell.GetName(), spell.GetEmoji(), formattedSpellDesc)
		}
		statuses := playerMons[currentTurn].GetStatus()
		if len(statuses) == 1 {
			desc += "Status: " + statuses[0].String() + "\n"
		} else if len(statuses) > 1 {
			desc += "Status: "
			for _, status := range statuses {
				desc += fmt.Sprintf("＀＀**%s**\n", status.String())
			}
		}
		stackStr := getStacksForMon(playerMons[currentTurn])
		if stackStr != "" {
			desc += "**Counters**:\n"
			desc += stackStr
		}
		desc += "\n**Turn Log**\n"
		len := log.Len()
		len = clamp(turnCount, len)
		log = log.Move(-len)
		for i := 0; i < len; i++ {
			name := fmt.Sprintf("Turn %d:", turnCount-len+i+1)
			value := log.Value.(string)
			log = log.Next()
			embed.AddField(name, value, false)
		}
		rollStr := "flipped no heads"
		if roll == 1 {
			rollStr = "flipped 1 head"
		} else if roll > 1 {
			rollStr = fmt.Sprintf("flipped %d heads", roll)
		}
		embed.SetFooter("", fmt.Sprintf("%s's turn - %s", players[currentTurn].String(), rollStr))
	}
	embed.SetDesc(desc)
	return embed
}

func makeFightImage(players []*discordgo.User, playerMons []mon.Monster, currentTurn int) image.Image {
	/*
		img := makeFightImage(playerMons, currentTurn)
		url, _ := utils.TryImgurMediaUpload()

	*/
	return nil
}

func makePlayerEmbed(players []*discordgo.User) *discordgo.MessageEmbed {
	embed := utils.NewEmbed().
		SetTitle("Lobby").
		SetDesc("Emote to join Lobby")
	fieldName := "**Players**:"
	fieldValue := ""
	for idx, player := range players {
		fieldValue += fmt.Sprintf("%d: %s", idx+1, player.String())
	}
	embed.AddField(fieldName, fieldValue, false)
	return embed.Get()

}

func getExpForBattle(turnCount, p1Level, p2Level, winner int) (int, int) {
	// 20 turns is max for exp consideration. multiple of 2 max
	baseTurnExp := 1 + float64(clamp(turnCount+1, 20))/20.
	// being winner multiplies exp by 1.5
	p1Mult, p2Mult := 1.0, 1.5
	if winner == 0 {
		p1Mult, p2Mult = 1.5, 1.0
	}
	averageLevel := float64(p1Level+p2Level) / 2.
	// exp is based on avg of levels and enemy's level
	// better to face equal level enemies for higher levels
	// low levels get boosted to higher levels
	// flat boost of turn count to get past earlier levels
	// will not scale nicely later, so best way is to fight higher levels
	p1Exp := averageLevel * baseTurnExp * float64(p2Level) * p1Mult
	p2Exp := averageLevel * baseTurnExp * float64(p1Level) * p2Mult
	return int(p1Exp) + turnCount, int(p2Exp) + turnCount
}
