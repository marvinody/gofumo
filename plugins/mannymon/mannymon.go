package mannymon

import (
	"fmt"
	"math/rand"
	"regexp"
	"strconv"
)

var (
	// Mons is a map of loaded Monsters, ready to be initialized
	Mons map[string]MonConstruct
	// Spells is available spells to use
	Spells map[string]SpellConstruct
)

// MonConstruct is the way I came up with to coalesce all the monsters
// I can construct one example one and then call name stuff on it to display
// Then just call this again when I need them for fighting
type MonConstruct func() Monster

// SpellConstruct has same idea as MonConstruct
type SpellConstruct func() Spell

func init() {
	Mons = map[string]MonConstruct{}
	Spells = map[string]SpellConstruct{}
}

// AddMon registers a mon in the global map
// Can call this or add directly, doesn't matter. Just convenience
func AddMon(name string, initializer MonConstruct) {
	Mons[name] = initializer
}

// AddSpell registers a spell in the global map
// MUST call this to add, otherwise your spell won't be registered
func AddSpell(name string, spell SpellConstruct) {
	Spells[name] = spell
}

// TurnStatus represents how the turn ended
type TurnStatus int

const (
	// TurnDone signifies the turn finished normally
	TurnDone TurnStatus = iota
	// TurnExtra means the monster should get an extra turn
	TurnExtra
)

// HookPhase is when some hook should occur, check each individual one for
// what's possible to change
// All can append to the template and add any needed data
// Care should be taken to ensure unique names though
type HookPhase int

const (
	// PreTurn goes before the attack is selected and can change the monster's roll
	PreTurn HookPhase = iota
	// PreAttack goes before the attack is executed and can change the outgoing damage
	PreAttack
	// PostAttack occurs after a mon has executed an attack, can't change anything
	PostAttack
	// PostTurn occurs right before ending one's turn, can change turnstatus
	PostTurn
	// PreHeal occurs before a mon heals. Can change the heal amt.
	PreHeal
	// PostHeal occurs after a mon heals
	PostHeal
)

// MonsterStatus reflects any status effects the monster may have
type MonsterStatus int

const (
	// StatusNormal is the default state
	StatusNormal MonsterStatus = iota
	// StatusCharmed means that any attempts to deal damage will turn into inflicting that damage onto self
	StatusCharmed
	// StatusInvulnerable will negate any damage being done
	StatusInvulnerable
	// StatusWounded indicates that the subject will receive 50% less healing
	StatusWounded
)

func (status MonsterStatus) String() string {
	switch status {
	case StatusCharmed:
		return "Charmed"
	case StatusInvulnerable:
		return "Invulnerable"
	default:
		return "Normal"
	}
}

type Template interface {
	Execute(MoveData) string
	AppendString(string) Template
	AppendTemplate(Template) Template
	GetRaw() string
}

type simpleTemplate struct {
	template string
}

var (
	// find something surrounded by {{ }}
	// that something MAY be preceded by a ".", but not needed
	// that can also be indexed normally, ie. "{{.nums[0]}}", however, nums must
	// be of type []int, otherwise the index is not successful
	templateSearcher = regexp.MustCompile(`{{\.?(\w+)(?:\[(\d+)\])?}}`)
)

func NewTemplate(template string) Template {
	return &simpleTemplate{
		template: template,
	}
}

func (t *simpleTemplate) Execute(data MoveData) string {
	var out string
	// we build up string from regexp searches, and we need to keep track of rightmost
	lastRight := 0
	// for any replacements we may need to do
	for _, match := range templateSearcher.FindAllStringSubmatchIndex(t.template, -1) {
		// 2,3 idxs contain slice that has first matched group, IE the key
		key := t.template[match[2]:match[3]]
		// anything preceding the previous match up to the leftmost of current match
		leftStr := t.template[lastRight:match[0]]
		// update for the next addition after to the outer of current match
		lastRight = match[1]
		// default thing to put if non-existent key
		var str interface{} = "<N/A>"
		if val, ok := data[key]; ok {
			if arr, ok := val.([]int); len(match) > 4 && ok {
				num, err := strconv.Atoi(t.template[match[4]:match[5]])
				if err == nil {
					val = arr[num]
				}
			}
			str = val
		}
		// not sure I like this, but %v is the easiest way to do it for any type?
		out += leftStr + fmt.Sprintf("%v", str)
	}
	out += t.template[lastRight:]
	return out
}

func (t *simpleTemplate) AppendString(extra string) Template {
	return &simpleTemplate{
		template: t.template + "\n" + extra,
	}
}

func (t *simpleTemplate) AppendTemplate(t2 Template) Template {
	return &simpleTemplate{
		template: t.template + "\n" + t2.GetRaw(),
	}
}

func (t *simpleTemplate) GetRaw() string {
	return t.template
}

// MoveData represents the data that will be injected into the move template
// You can add any fields needed for yourself
// Some will be added after you return it, so you can reference the following
// in your template without worrying about defining them
// {{.self}} -> Monster name and player name
// {{.damage}} -> Damage done by the attack. NOT the original amount, but the actual done
// {{.heal}} -> amt HP healed. NOT the original amount (if any) but actual increase in hp
// {{.enemy}} -> Similar to self but enemy
// {{.nums}} -> nums from move, still like an array
// {{enemyDamage}} -> Damage by enemy to self
// {{enemyHeal}} -> same as heal and prior
type MoveData map[string]interface{}

// Monster represents all the functions/data we'll need to simulate a mon
// BaseMonster provides an implementation for these, so you won't have to
// implement them yourself. I highly recommend NOT implementing your own mon
// from the ground up because there's a bunch of hidden stuff that would make it
// incompatible. Simple embed BaseMonster in your mon and overwrite Init, and
// GetImage. GetImage can return null for now, and look for a sample of Init in
// other mons. Calling New from this package wouldn't be a bad idea as it does
// some boilerplate for you in creating needed internals
type Monster interface {
	Init(Monster, *Options)
	GetName() string
	GetEpithet() string
	GetDesc() string
	GetHP() int
	Heal(Monster, int, *Options)
	TakeDamage(Monster, int, *Options)
	DealDamage(Monster, int, *Options)
	GetMoves() []Move
	SetMove(int, Move)
	DoMove(int, Monster, *Options) (TurnStatus, Template, MoveData)
	AddCounter(string, Counter) Counter
	GetCounter(string) Counter
	GetCounters() map[string]Counter
	AddHook(HookPhase, Hook)
	AddStatus(MonsterStatus)
	HasStatus(MonsterStatus) bool
	RemoveStatus(MonsterStatus)
	GetStatus() []MonsterStatus
	GetImage() []byte
	Roll(*Options) int
}

// Options is a general purpose passed into a lot of methods
//
type Options struct {
	TurnCount  int
	RNG        *rand.Rand
	IsSpell    bool
	MoveRolled int
	MovePicked int
}

// Hook is a function that will activate at some phase.
// It returns a priority rating (should be from 0 to 1) and modified num if needed
// A priority of 0 means num should not have been changed
// It doesn't always make sense to return something, like for instance
// in most post hook. But I don't see a reason to split them up
// Hook will always receive the original num even if an earlier hook modified it
// The one with the highest priority will be kept and the rest discarded though
type Hook func(self, enemy Monster, num int, opts *Options) *HookData

// HookData lets you change some stuff when returned in a hook
// Not everything is relevant, check the HookPhase for what can be changed
type HookData struct {
	Priority float64
	Num      int

	TemplateExtension string
	TemplateData      MoveData
}

// Move is a wrapper for a monmove
// Func should be the actual implementation
// Desc should be a string that can be formatted by sprintf when called with nums
type Move interface {
	GetFunc() MoveFunc
	GetDesc() string
	GetName() string
	GetNums() []int
}

// Spell is essentially a move, but we're classifying them differently
// Since they're used outside of moves
type Spell interface {
	Move
	GetEmoji() string
	IsUsable() bool
}

// MoveFunc is almost like a pointer method, in the sense that it should act on the first variable
// We do this because we can interchange moves if needed and it makes them less tied to each mon
type MoveFunc func(attacking, defending Monster, opt *Options) (TurnStatus, Template, MoveData)

// BaseMove is a simple implementation of Move
// You can embed if needed in your class, or just use normally
// Desc should have string formatting directives for each num
// Func should use Nums as opposed to hardcoding them, but this is not enforced
type BaseMove struct {
	Func MoveFunc
	Desc string
	Name string
	Nums []int
}

func (move *BaseMove) GetFunc() MoveFunc {
	return move.Func
}
func (move *BaseMove) GetDesc() string {
	return move.Desc
}
func (move *BaseMove) GetName() string {
	return move.Name
}
func (move *BaseMove) GetNums() []int {
	return move.Nums
}

// NewMove is a helper function used in mons to create their moveset
// Name and Desc are what will be shown in the help/fight pages
// TemplateString will be compiled and then have data injected and shown in the log
// Wrapper will be passed the Nums slice so you can just use one int slice for the move
// When desc is displayed, it will have nums put into them by regular order
func NewMove(Name, Desc, TemplateString string, Nums []int, wrapper func(Nums []int, t Template) MoveFunc) *BaseMove {
	move := wrapper(Nums, NewTemplate(TemplateString))
	return &BaseMove{
		Name: Name,
		Desc: Desc,
		Nums: Nums,
		Func: move,
	}
}

// BaseMonster is a simple implementation of Monster
// You will almost certainly need to embed this or create your own
// Check out cerberos or devilbat for sample
type BaseMonster struct {
	HP      int
	Name    string
	Epithet string
	Desc    string

	Moves    []Move
	Counters map[string]Counter

	Hooks map[HookPhase][]Hook

	Statuses map[MonsterStatus]bool

	curTurnTemplate     Template
	curTurnTemplateData MoveData
}

func New(name, epithet, desc string, hp int) *BaseMonster {
	mon := &BaseMonster{
		HP:      hp,
		Name:    name,
		Epithet: epithet,
		Desc:    desc,
	}
	mon.Hooks = map[HookPhase][]Hook{}
	mon.Counters = map[string]Counter{}
	mon.Statuses = map[MonsterStatus]bool{}
	return mon
}

// Init just initializes the hook and counter maps currently
func (mon *BaseMonster) Init(_ Monster, _ *Options) {

}
func (mon *BaseMonster) GetName() string {
	return mon.Name
}

func (mon *BaseMonster) GetEpithet() string {
	return mon.Epithet
}

func (mon *BaseMonster) GetDesc() string {
	return mon.Desc
}

// GetHP has some peculiar behavior
// if HP is negative (perhaps from recent attack), this will return
// a 0 in that case
func (mon *BaseMonster) GetHP() int {
	if mon.HP < 0 {
		return 0
	}
	return mon.HP
}
func (mon *BaseMonster) Heal(enemy Monster, healAmt int, opts *Options) {
	preHooks := mon.Hooks[PreHeal]
	postHooks := mon.Hooks[PostHeal]
	healData := HandleHooks(preHooks, mon, enemy, &HookData{
		Num:      healAmt,
		Priority: 0,
	}, opts)

	mon.HP += healData.Num
	if healData.Priority > 0 {
		mon.curTurnTemplate = mon.curTurnTemplate.AppendString(healData.TemplateExtension)
		addToMoveData(mon.curTurnTemplateData, healData.TemplateData)
	}
	HandleHooks(postHooks, mon, enemy, healData, opts)
}

// TakeDamage causes mon to take certain damage. Does not harm enemy
func (mon *BaseMonster) TakeDamage(enemy Monster, dmgAmt int, opts *Options) {
	mon.HP -= dmgAmt
}

// DealDamage causes damage to enemy monster. Does not self inflict damage
// Will call TakeDamage on enemy
func (mon *BaseMonster) DealDamage(enemy Monster, dmgAmt int, opts *Options) {
	preHooks := mon.Hooks[PreAttack]
	postHooks := mon.Hooks[PostAttack]
	dmgData := HandleHooks(preHooks, mon, enemy, &HookData{
		Num:      dmgAmt,
		Priority: 0,
	}, opts)

	enemy.TakeDamage(mon, dmgData.Num, opts)
	if dmgData.Priority > 0 {
		mon.curTurnTemplate = mon.curTurnTemplate.AppendString(dmgData.TemplateExtension)
		addToMoveData(mon.curTurnTemplateData, dmgData.TemplateData)
	}
	HandleHooks(postHooks, mon, enemy, dmgData, opts)
}

func (mon *BaseMonster) GetMoves() []Move {
	return mon.Moves
}

func (mon *BaseMonster) SetMove(idx int, move Move) {
	if idx >= len(mon.Moves) {
		return
	}
	mon.Moves[idx] = move
}

func (mon *BaseMonster) DoMove(idx int, enemy Monster, opts *Options) (TurnStatus, Template, MoveData) {
	// reset these to use in other functions
	mon.curTurnTemplate = &simpleTemplate{}
	mon.curTurnTemplateData = MoveData{}
	// save for later
	preHooks := mon.Hooks[PreTurn]
	postHooks := mon.Hooks[PostTurn]
	// pre turn hook can modify the roll
	rollData := HandleHooks(preHooks, mon, enemy, &HookData{
		Num:      idx,
		Priority: 0,
	}, opts)
	// we only append shit if something had a priority
	if rollData.Priority > 0 {
		mon.curTurnTemplate = mon.curTurnTemplate.AppendString(rollData.TemplateExtension)
		addToMoveData(mon.curTurnTemplateData, rollData.TemplateData)
	}
	move := mon.Moves[rollData.Num]
	// the actual move function (tiny line)
	status, moveTemplate, moveData := move.GetFunc()(mon, enemy, opts)
	// post turn hooks can modify turn status
	turnData := HandleHooks(postHooks, mon, enemy, &HookData{
		Num:      int(status),
		Priority: 0,
	}, opts)
	if turnData.Priority > 0 {
		mon.curTurnTemplate = mon.curTurnTemplate.AppendString(turnData.TemplateExtension)
		addToMoveData(mon.curTurnTemplateData, turnData.TemplateData)
	}
	if moveData == nil {
		moveData = MoveData{}
	}
	// and anything that hooks may have added/changed go after the original message
	finalTemplate := moveTemplate.AppendTemplate(mon.curTurnTemplate)
	addToMoveData(moveData, mon.curTurnTemplateData)
	return TurnStatus(turnData.Num), finalTemplate, moveData
}
func (mon *BaseMonster) AddCounter(name string, counter Counter) Counter {
	mon.Counters[name] = counter
	return counter
}
func (mon *BaseMonster) GetCounter(name string) Counter {
	return mon.Counters[name]
}
func (mon *BaseMonster) GetCounters() map[string]Counter {
	return mon.Counters
}
func (mon *BaseMonster) AddHook(phase HookPhase, hook Hook) {
	mon.Hooks[phase] = append(mon.Hooks[phase], hook)
}
func (mon *BaseMonster) RemoveStatus(status MonsterStatus) {
	delete(mon.Statuses, status)
}
func (mon *BaseMonster) AddStatus(status MonsterStatus) {
	// we don't want to add this because normal is the absence of everything else
	if status == StatusNormal {
		return
	}
	mon.Statuses[status] = true
}
func (mon *BaseMonster) HasStatus(status MonsterStatus) bool {
	// mon's status is only normal if no status effects otherwise
	if status == StatusNormal {
		return len(mon.Statuses) == 0
	}
	_, has := mon.Statuses[status]
	return has
}
func (mon *BaseMonster) GetStatus() []MonsterStatus {
	statuses := make([]MonsterStatus, 0, len(mon.Statuses))
	for status := range mon.Statuses {
		statuses = append(statuses, status)
	}
	return statuses
}
func (mon *BaseMonster) GetImage() []byte {
	return nil
}
func (mon *BaseMonster) Roll(opts *Options) int {
	moveCount := len(mon.GetMoves())
	return ConsecHeads(opts.RNG, moveCount)
}

// ConsecHeads returns the number of heads in range [0, max)
func ConsecHeads(rng *rand.Rand, max int) int {
	cnt := 0
	// [0,2) -> [0,1]. 0 is tails, 1 is heads
	for rng.Intn(2) > 0 && cnt+1 < max {
		cnt++
	}
	return cnt
}

// Counter can be used to represent simple counters with weird logic
type Counter interface {
	// Get returns the internal counter
	Get() int
	// Inc increases the counter by n
	Inc(n int)
	// Dec decreases the counter by n
	Dec(n int)
	// Refreshes the internal counter based on the implementation
	Refresh(n int)
	// Reset sets the counter to 0
	Reset()
	// Name returns the name of the counter (may not be the same as the key)
	Name() string
}

type simpleCounter struct {
	Count int
	name  string
}

func (c *simpleCounter) Get() int {
	return c.Count
}

func (c *simpleCounter) Inc(n int) {
	c.Count += n
}

func (c *simpleCounter) Dec(n int) {
	c.Count -= n
}

func (c *simpleCounter) Name() string {
	return c.name
}

func (c *simpleCounter) Reset() {
	c.Count = 0
}

// StackingCounter will increase by n on each refresh, without limit
type StackingCounter struct {
	*simpleCounter
}

// Refresh will increase the counter by n each call, without limit
func (c *StackingCounter) Refresh(n int) {
	c.Count += n
}

func NewStackingCounter(name string) *StackingCounter {
	return &StackingCounter{
		&simpleCounter{
			name: name,
		},
	}
}

// RefreshingCounter will refresh the counter to n, without going over
type RefreshingCounter struct {
	*simpleCounter
}

// Refresh will set the counter to n
func (c *RefreshingCounter) Refresh(n int) {
	c.Count = n
}

func NewRefreshingCounter(name string) *RefreshingCounter {
	return &RefreshingCounter{
		&simpleCounter{
			name: name,
		},
	}
}

// HandleHooks will run each hook for specified inputs and return the final result
func HandleHooks(hooks []Hook, self, enemy Monster, origData *HookData, opts *Options) *HookData {
	data := origData
	for _, hook := range hooks {
		newData := hook(self, enemy, origData.Num, opts)
		if newData.Priority > data.Priority {
			data = newData
		}
	}
	return data
}

func addToMoveData(origData, newData MoveData) {
	if newData == nil {
		return
	}
	for k, v := range newData {
		origData[k] = v
	}
}
