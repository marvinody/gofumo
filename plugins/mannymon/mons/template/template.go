package succubus

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Monster struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Monster{
		mannymon.New(
			"Name",
			"Epithet",
			"Desc",
			0,
		),
	}
	return m
}

func (m *Monster) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)

	// sample hook doing nothing
	enemy.AddHook(mannymon.PreAttack, func(attacking mannymon.Monster, defending mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
		return &mannymon.HookData{
			Num:      num,
			Priority: 0,
		}
	})

	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Move Name",
			"Move Desc",
			"{{self}} hit {{enemy}} for {{damage}}",
			[]int{1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, 2, opts)
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (m *Monster) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("Name", New)
}
