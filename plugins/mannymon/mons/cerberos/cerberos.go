package cerberos

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Cerberos struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Cerberos{
		mannymon.New(
			"Cerberos",
			"One of the original two mons",
			"Medium damage, but has an instagib move",
			12,
		),
	}
	return m
}

func (c *Cerberos) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	c.BaseMonster.Init(enemy, opts)

	// this goes here so we don't have to create 2
	biteMove := mannymon.NewMove(
		"Bite",
		"bites the enemy for %d damage",
		"{{.self}} bit {{.enemy}} for {{.damage}} HP",
		[]int{3},
		func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
			return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
				self.DealDamage(enemy, Nums[0], opts)
				return mannymon.TurnDone, t, nil
			}
		})

	c.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Splash",
			"does absolutely nothing",
			"{{.self}} flailed around and did nothing",
			[]int{},
			func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					return mannymon.TurnDone, t, nil
				}
			}),
		biteMove,
		biteMove,
		mannymon.NewMove(
			"Claw",
			"scratches the enemy for %d damage",
			"{{.self}} scratched {{.enemy}} for {{.damage}} HP",
			[]int{5},
			func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, Nums[0], opts)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Swallow",
			"Deals damage equal to enemy's current HP",
			"{{.self}} swallowed {{.enemy}} for {{.damage}} HP",
			[]int{},
			func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, enemy.GetHP(), opts)
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (c *Cerberos) GetImage() []byte {
	return ImgData
}

// CerbMove is an example of how to do dynamic damage of moves and text
type CerbMove struct {
	*mannymon.BaseMove
	wrapper func([]int) mannymon.MoveFunc
}

func (cm *CerbMove) GetFunc() mannymon.MoveFunc {
	return cm.wrapper(cm.GetNums())
}

func init() {
	mannymon.AddMon("cerberos", New)
}
