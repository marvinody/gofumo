package muscles

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Muscles struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Muscles{
		mannymon.New(
			"Muscles",
			"The Swole Mon",
			"Build up your stacks and unleash a high damaging move",
			17,
		),
	}
	return m
}

func (m *Muscles) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)

	m.AddCounter("extra_dmg", mannymon.NewStackingCounter("Stacks"))
	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Cardio",
			"Adds %d to stacks",
			"{{.self}} steps on a treadmill and gains +{{.nums[0]}} to stacks",
			[]int{1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.GetCounter("extra_dmg").Refresh(Nums[0])
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Weightlifting",
			"Adds %d to stacks",
			"{{.self}} does some serious reps and gains +{{.nums[0]}} stacks",
			[]int{2}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.GetCounter("extra_dmg").Refresh(Nums[0])
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"X gon' give it to you",
			"Deals %d base + stacks damage. Resets stacks",
			"{{.self}} smacks {{.enemy}} with his cane, dealing {{.damage}} damage",
			[]int{3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					counter := self.GetCounter("extra_dmg")
					stacks := counter.Get()
					counter.Reset()
					self.DealDamage(enemy, Nums[0]+stacks, opts)
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (m *Muscles) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("muscles", New)
}
