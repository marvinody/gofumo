package zenturtle

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type ZenTurtle struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &ZenTurtle{
		mannymon.New(
			"ZenTurtle",
			"Rest and Tranquility",
			"Lots of heals...and recoil damage unless you activate his invulnerability",
			12,
		),
	}
	return m
}

var (
	headbuttInvuln    = mannymon.NewTemplate("{{.self}} headbutted {{.enemy}} for {{.damage}} damage")
	headbuttNoInvuln  = mannymon.NewTemplate("{{.self}} headbutted {{.enemy}} for {{.damage}} damage, but also received {{.recoil}} recoil damage")
	shellSpinInvuln   = mannymon.NewTemplate("{{.self}} hit {{.enemy}} for {{.damage}} damage")
	shellSpinNoInvuln = mannymon.NewTemplate("{{.self}} hit {{.enemy}} for {{.damage}} damage, but also received {{.recoil}} recoil damage")
)

func (m *ZenTurtle) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)

	invulnCounter := m.AddCounter("invuln", mannymon.NewRefreshingCounter("Invuln. Turns "))
	// decay hook
	enemy.AddHook(mannymon.PostTurn, func(self mannymon.Monster, enemy mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
		if invulnCounter.Get() > 0 {
			invulnCounter.Dec(1)
		}
		// remove invuln
		if invulnCounter.Get() == 0 {
			m.RemoveStatus(mannymon.StatusInvulnerable)
		}
		return &mannymon.HookData{
			Num:      num,
			Priority: 0,
		}
	})

	enemy.AddHook(mannymon.PreAttack, func(attacking mannymon.Monster, defending mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
		if defending.HasStatus(mannymon.StatusInvulnerable) {
			return &mannymon.HookData{
				Num:               0,
				Priority:          1,
				TemplateExtension: "{{.enemy}} was invulnerable",
			}
		}
		return &mannymon.HookData{
			Num: num,
		}
	})

	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Rest",
			"Relax to heal for %d",
			"{{.self}} rested and healed {{.heal}} HP",
			[]int{3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.Heal(enemy, Nums[0], opts)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Headbutt",
			"Hit for %d, but receive recoil damage for %d",
			"",
			[]int{4, 3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					t = headbuttInvuln
					if invulnCounter.Get() == 0 {
						t = headbuttNoInvuln
						self.TakeDamage(self, Nums[1], opts)
					}
					self.DealDamage(enemy, Nums[0], opts)
					return mannymon.TurnDone, t, map[string]interface{}{
						"recoil": Nums[1],
					}
				}
			}),
		mannymon.NewMove(
			"Shellspin",
			"Hit for %d, but receive recoil damage for %d",
			"",
			[]int{5, 4}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					t = shellSpinInvuln
					if invulnCounter.Get() == 0 {
						t = shellSpinNoInvuln
						self.TakeDamage(self, Nums[1], opts)
					}
					self.DealDamage(enemy, Nums[0], opts)
					return mannymon.TurnDone, t, map[string]interface{}{
						"recoil": Nums[1],
					}
				}
			}),
		mannymon.NewMove(
			"Cowabunga",
			"Receive invulnerability for %d turns\nUsing again before expiration will only refresh, not stack",
			"Cowabunga dude! {{.self}} gained invulnerability for {{.invulnCount}} turns",
			[]int{4}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					invulnCounter.Refresh(Nums[0])
					self.AddStatus(mannymon.StatusInvulnerable)
					return mannymon.TurnDone, t, map[string]interface{}{
						"invulnCount": invulnCounter.Get(),
					}
				}
			}),
	}
}

func (m *ZenTurtle) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("zenturtle", New)
}
