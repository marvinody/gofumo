package devilbat

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Devilbat struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Devilbat{
		mannymon.New(
			"Devilbat",
			"One of the original two",
			"Meh damage, but can get extra turns and large heal",
			10,
		),
	}
	return m
}

func (d *Devilbat) Init(enemy mannymon.Monster, opts *mannymon.Options) {
	d.BaseMonster.Init(enemy, opts)
	d.Moves = []mannymon.Move{
		&mannymon.BaseMove{
			Func: chop,
			Name: "Wing Chop",
			Desc: "1 Damage",
		},
		&mannymon.BaseMove{
			Func: fire,
			Name: "Fire",
			Desc: "2 Damage",
		},
		&mannymon.BaseMove{
			Func: hypno,
			Name: "Hypnotic Eye Beam",
			Desc: "2 Damage + 1 Extra Attack",
		},
		&mannymon.BaseMove{
			Func: bloodsuck,
			Name: "Bloodsucking",
			Desc: "Lifesteal 4 damage",
		},
	}
}

var (
	chop_template      = mannymon.NewTemplate("{{.self}} hit {{.enemy}} for {{.damage}} HP")
	fire_template      = mannymon.NewTemplate("{{.self}} sets {{.enemy}} on fire for {{.damage}} HP")
	hypno_template     = mannymon.NewTemplate("{{.self}} hypnotizes {{.enemy}}, dealing {{.damage}} damage and getting another turn")
	bloodsuck_template = mannymon.NewTemplate("{{.self}} sucks the lifeforce of {{.enemy}} dealing {{.damage}} damage and recovering {{.heal}}")
)

func (db *Devilbat) GetImage() []byte {
	return imgdata
}

func chop(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
	self.DealDamage(enemy, 1, opts)
	return mannymon.TurnDone, chop_template, nil
}

func fire(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
	self.DealDamage(enemy, 2, opts)
	return mannymon.TurnDone, fire_template, nil
}

func hypno(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
	self.DealDamage(enemy, 2, opts)
	return mannymon.TurnExtra, hypno_template, nil
}

func bloodsuck(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
	self.DealDamage(enemy, 4, opts)
	self.Heal(enemy, 4, opts)
	return mannymon.TurnDone, bloodsuck_template, nil
}

func init() {
	mannymon.AddMon("devilbat", New)
}
