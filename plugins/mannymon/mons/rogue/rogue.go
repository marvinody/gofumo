package succubus

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Monster struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Monster{
		mannymon.New(
			"Rogue",
			"The Dark Lord",
			"Varied moves that work with a base stacking mechanic",
			10,
		),
	}
	return m
}

func (m *Monster) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)

	sneak := m.AddCounter("sneak", mannymon.NewRefreshingCounter("Sneak"))

	// sample hook doing nothing
	enemy.AddHook(mannymon.PreTurn, func(attacking mannymon.Monster, defending mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {

		return &mannymon.HookData{
			Num:      num,
			Priority: 0,
		}
	})

	splash := mannymon.NewMove(
		"Splash",
		"Does nothing (set by Rogue)",
		"{{self}} splashed around",
		[]int{}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
			return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
				return mannymon.TurnDone, t, nil
			}
		})

	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Stealth",
			"Grants %d sneak, but receive %d damage. If you already have sneak stacks, gain %d instead with no damage",
			"{{self}} gained {{cnt}} sneak stacks",
			[]int{4, 1, 3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					if sneak.Get() > 0 {
						sneak.Inc(Nums[2])
						return mannymon.TurnDone, t, mannymon.MoveData{
							"cnt": Nums[2],
						}
					}

					sneak.Inc(Nums[0])
					self.TakeDamage(enemy, Nums[1], opts)
					return mannymon.TurnDone,
						mannymon.NewTemplate("{{self}} gained {{cnt}} sneak stacks but took {{enemyDamage}} damage"),
						mannymon.MoveData{
							"cnt": Nums[0],
						}
				}
			}),
		mannymon.NewMove(
			"Samehada",
			"Deals %d damage. Will consume %d sneak stack to deal %d extra damage. Heals for same amount",
			"{{self}} hit {{enemy}} for {{damage}}, and healed for {{heal}}",
			[]int{1, 1, 1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					dmg := Nums[0]
					if sneak.Get() > 0 {
						sneak.Dec(Nums[1])
						dmg += Nums[2]
					}
					self.DealDamage(enemy, dmg, opts)
					self.Heal(enemy, dmg, opts)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Dumplings",
			"Heals for %d HP. Will consume %d sneak stack to heal %d extra HP",
			"{{self}} healed for {{heal}} HP",
			[]int{2, 1, 1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					heal := Nums[0]
					if sneak.Get() > 0 {
						sneak.Dec(Nums[1])
						heal += Nums[2]
					}
					self.Heal(enemy, heal, opts)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Substitution Jutsu",
			"Replaces the enemy's highest roll move with Splash (which does nothing)",
			"{{self}} replaced {{enemy}}'s highest move",
			[]int{}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					lastEnemyMove := len(enemy.GetMoves()) - 1
					enemy.SetMove(lastEnemyMove, splash)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Shadow Stars",
			"If you have nonzero sneak, consumes all stacks to deal %d + %d*stacks damage",
			"{{self}} hit {{enemy}} for {{damage}} HP",
			[]int{2, 2}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					stacks := sneak.Get()
					if stacks > 0 {
						sneak.Dec(stacks)
						self.DealDamage(enemy, Nums[0]+stacks*Nums[1], opts)
					}
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (m *Monster) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("rogue", New)
}
