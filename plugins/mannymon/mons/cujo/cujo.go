package cujo

import (
	"strings"

	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Cujo struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Cujo{
		mannymon.New(
			"Cujo",
			"The Lone Wolf",
			"Has damage reduction, healing, and a devasting move at low HP",
			11,
		),
	}
	return m
}

func (m *Cujo) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)

	dmgRdcNums := []int{3, 1}
	dmgRdc := m.AddCounter("dmg_rdc", mannymon.NewStackingCounter("Dmg. Reduction"))
	// hook to decrement counter each turn after enemy's turn
	enemy.AddHook(mannymon.PostTurn, func(_, _ mannymon.Monster, num int, _ *mannymon.Options) *mannymon.HookData {
		if dmgRdc.Get() > 0 {
			// reduce it each turn if above 0.
			// even if not used, it decays
			dmgRdc.Dec(1)
		}
		return &mannymon.HookData{
			Num:      num,
			Priority: 0,
		}
	})
	// hook to reduce damage each turn if possible
	enemy.AddHook(mannymon.PreAttack, func(_, _ mannymon.Monster, origNum int, _ *mannymon.Options) *mannymon.HookData {
		if dmgRdc.Get() > 0 {
			num := origNum
			num -= dmgRdcNums[0]
			if num < 0 {
				num = 0
			}
			return &mannymon.HookData{
				Num:               num,
				Priority:          1,
				TemplateExtension: "{{enemy}}'s Howl reduced the damage by {{hook_reduced}}!",
				TemplateData: map[string]interface{}{
					"hook_reduced": origNum - num,
				},
			}
		}
		return &mannymon.HookData{
			Num:      origNum,
			Priority: 0,
		}
	})
	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Howl",
			"Gives %d damage reduction for the next %d turn.",
			"{{.self}} howled, giving {{.nums[0]}} damage reduction for {{.nums[1]}} turn(s)",
			dmgRdcNums, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.GetCounter("dmg_rdc").Refresh(Nums[1])
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Bite",
			"Deals %d damage",
			"{{.self}} bit {{.enemy}} for {{.damage}} HP",
			[]int{2}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, Nums[0], opts)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Devour",
			"Deals %d damage and heals back %d HP",
			"{{.self}} feasts on {{.enemy}} dealing {{.damage}} damage and recovering {{.heal}}",
			[]int{3, 2}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, Nums[0], opts)
					self.Heal(enemy, Nums[1], opts)
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Bloodrush",
			strings.Join([]string{
				"Deals increasing damage the lower hp you have:",
				"＀＀• <= %d HP, %d damage",
				"＀＀• <= %d HP, %d damage",
				"＀＀• Otherwise, %d damage",
			}, "\n"),
			"{{.self}} attacks ferociously, dealing {{.damage}} to {{.enemy}}",
			[]int{5, 12, 7, 8, 5}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					hp := m.HP
					var dmg int
					if hp <= Nums[0] {
						dmg = Nums[1]
					} else if hp <= Nums[2] {
						dmg = Nums[3]
					} else {
						dmg = Nums[4]
					}
					self.DealDamage(enemy, dmg, opts)
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (m *Cujo) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("cujo", New)
}
