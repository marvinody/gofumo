package succubus

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Succubus struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Succubus{
		mannymon.New(
			"Succubus",
			"Irresistibly charming :revolving_hearts: ",
			"Can become invulnerable and charm the enemy mon to deal damage to itself",
			9,
		),
	}
	return m
}

const (
	invulnPriority = 0.5
	charmPriority  = 1.0
)

const (
	charmName = "charms"

	consecHeadsNeededToBreakCharm = 2
)

func (m *Succubus) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)
	charmCounter := enemy.AddCounter(charmName, mannymon.NewRefreshingCounter("Charm Counter"))
	// deal with charm after each enemy turn
	enemy.AddHook(mannymon.PostTurn, func(attacking mannymon.Monster, defending mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
		if attacking.HasStatus(mannymon.StatusCharmed) {
			roll := mannymon.ConsecHeads(opts.RNG, consecHeadsNeededToBreakCharm+1)
			if roll == consecHeadsNeededToBreakCharm {
				attacking.RemoveStatus(mannymon.StatusCharmed)
				return &mannymon.HookData{
					Num:               num,
					Priority:          0.1,
					TemplateExtension: "{{self}} broke free from {{enemy}}'s charm!",
				}
			}
		}
		return &mannymon.HookData{
			Num:      num,
			Priority: 0,
		}
	})

	// invuln status
	// if enemy charmed and self invuln, the charm will take priority
	enemy.AddHook(mannymon.PreAttack, func(attacking mannymon.Monster, defending mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
		if !defending.HasStatus(mannymon.StatusInvulnerable) {
			return &mannymon.HookData{
				Num:      num,
				Priority: 0,
			}
		}
		// so we're invuln, we give it lower priority so charm can take over
		// and we're gonna remove invuln even if user charmed because OP otherwise?
		defending.RemoveStatus(mannymon.StatusInvulnerable)
		return &mannymon.HookData{
			Num:               0,
			Priority:          invulnPriority,
			TemplateExtension: "{{enemy}} was invulnerable",
		}
	})

	// deal with charmed status
	enemy.AddHook(mannymon.PreAttack, func(attacking mannymon.Monster, defending mannymon.Monster, num int, opts *mannymon.Options) *mannymon.HookData {
		// if not charmed, just not gonna touch anything
		if !attacking.HasStatus(mannymon.StatusCharmed) {
			return &mannymon.HookData{
				Num:      num,
				Priority: 0,
			}
		}

		defending.DealDamage(attacking, num, opts)

		return &mannymon.HookData{
			Num:               0,
			Priority:          charmPriority,
			TemplateExtension: "{{self}} was charmed and lost {{enemyDamage}} HP",
		}

	})

	attemptCharm := func(self, enemy mannymon.Monster) {
		if enemy.GetCounter(charmName).Get() == 3 {
			enemy.AddStatus(mannymon.StatusCharmed)
		}
	}

	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Wink",
			"Increase charm counter by %d (max 3).",
			"{{self}} winked at {{enemy}} (charm increased to {{charmCnt}})",
			[]int{1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					if charmCounter.Get() == 3 {
						return mannymon.TurnDone, mannymon.NewTemplate("{{self}} winked at {{enemy}} (but nothing happened)"), nil
					}
					charmCounter.Inc(Nums[0])
					return mannymon.TurnDone, t, mannymon.MoveData{
						"charmCnt": charmCounter.Get(),
					}
				}
			}),
		mannymon.NewMove(
			"Hug",
			"Deal %d damage, add %d to charm counter, and attempt to charm.",
			"{{self}} hugs {{enemy}}, dealing {{damage}} and charming them",
			[]int{1, 1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, Nums[0], opts)
					// so we have some different scenarios here
					// some or no charms
					// max charms
					// already charmed
					// this will all be considered exclusive

					if enemy.HasStatus(mannymon.StatusCharmed) {
						// just dont bother with saying charms if already charmed
						return mannymon.TurnDone, mannymon.NewTemplate("{{self}} hugs {{enemy}}, dealing {{damage}}"), nil
					}
					charmCounter.Inc(Nums[1])
					attemptCharm(self, enemy)
					if charmCounter.Get() < 3 {
						return mannymon.TurnDone,
							mannymon.NewTemplate("{{self}} hugs {{enemy}}, dealing {{damage}}, increasing charm count to {{charmCnt}}"),
							mannymon.MoveData{
								"charmCnt": charmCounter.Get(),
							}
					}
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Kiss",
			"Increase charm count by %d, attempt to charm, deal %d damage, and become invulnerable to the next attack.",
			"{{.self}} failed to charm, but dealt {{damage}} HP of damage and became invulnerable to the next attack",
			[]int{1, 3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					if charmCounter.Get() < 3 {
						charmCounter.Inc(Nums[0])
					}
					attemptCharm(self, enemy)
					self.DealDamage(enemy, Nums[1], opts)
					self.AddStatus(mannymon.StatusInvulnerable)
					// default msg
					if charmCounter.Get() < 3 {
						return mannymon.TurnDone, t, nil
					}
					// is 3 and got charmed/is charmed
					return mannymon.TurnDone,
						mannymon.NewTemplate("{{self}} charmed {{enemy}} and hit them for {{damage}}, also becoming invulnerable to the next attack"), nil

				}
			}),
		mannymon.NewMove(
			"Hold Hands",
			"Deal %d damage and charm the enemy (regardless of charm count)",
			"{{self}} hit for {{damage}} HP and charmed {{enemy}}",
			[]int{4}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, Nums[0], opts)
					enemy.AddStatus(mannymon.StatusCharmed)
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (m *Succubus) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("succubus", New)
}
