package geomon

import (
	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Geomon struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Geomon{
		mannymon.New(
			"Geomon",
			"Geodude looking motherf**ker",
			"One unchanging move and does low damage",
			12,
		),
	}
	return m
}

func (g *Geomon) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	g.BaseMonster.Init(enemy, opts)
	g.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Rock Smash",
			"Deals %d damage.",
			"{{.self}} smashes {{.enemy}} with a rock, dealing {{.damage}} damage",
			[]int{2}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					enemy.DealDamage(enemy, Nums[0], opts)
					return mannymon.TurnDone, t, nil
				}
			}),
	}
}

func (g *Geomon) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("geomon", New)
}
