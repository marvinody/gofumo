package ratking

import (
	"strings"

	"bitbucket.org/marvinody/gofumo/plugins/mannymon"
)

type Ratking struct {
	*mannymon.BaseMonster
}

func New() mannymon.Monster {
	m := &Ratking{
		mannymon.New(
			"Ratking",
			"Speak softly and carry a big rat-stick",
			"Complex move conditions but large paypoffs if you can manage them",
			17,
		),
	}
	return m
}

var (
	mutatedRatGoonsT1 = mannymon.NewTemplate("{{.self}} recruited {{.ratNum}} rats, dealt {{.damage}} damage, and managed to get an extra turn")
	mutatedRatGoonsT2 = mannymon.NewTemplate("{{.self}} lost {{.ratNum}} rats, but dealt {{.damage}} damage")
	mutatedRatGoonsT3 = mannymon.NewTemplate("{{.self}} used up all his rats to deal {{.damage}} damage to {{.enemy}}")
	ratRevT           = mannymon.NewTemplate("{{.self}} dealt {{.damage}} damage to {{.enemy}}")
)

func (m *Ratking) Init(enemy mannymon.Monster, opts *mannymon.Options) {

	m.BaseMonster.Init(enemy, opts)

	ratCounter := m.AddCounter("rats", mannymon.NewStackingCounter("Rats"))
	m.Moves = []mannymon.Move{
		mannymon.NewMove(
			"Recruit",
			"Adds %d to rats",
			"{{.self}} recruited {{.nums[0]}} rats to his cause",
			[]int{3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					ratCounter.Refresh(Nums[0])
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Rat Bash",
			"Deals %d damage and add %d to rats",
			"{{.self}} recruited {{.nums[0]}} more to his cause and dealt {{.damage}} damage to {{.enemy}}",
			[]int{1, 1}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					self.DealDamage(enemy, Nums[0], opts)
					ratCounter.Refresh(Nums[1])
					return mannymon.TurnDone, t, nil
				}
			}),
		mannymon.NewMove(
			"Rat Goons",
			strings.Join([]string{
				"Depending on rat count:",
				"＀＀• 0 rats: Increase rats by %d, and deal %d damage",
				"＀＀• 1 rat: Decrease rats by %d, and deal %d damage",
				"＀＀• 2 or more rats: Changes rat count by %d, and deal %d damage",
			}, "\n"),
			"{{.self}} {{.keyword}} {{.num}} rats and dealt {{.damage}} damage to {{.enemy}}",
			[]int{1, 2, -1, 4, -2, 6}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					var startIdx int
					moveData := map[string]interface{}{
						"keyword": "gained",
						"num":     Nums[4],
					}
					switch {
					case ratCounter.Get() == 0:
						startIdx = 0
						moveData["num"] = Nums[startIdx]
					case ratCounter.Get() == 1:
						startIdx = 2
						moveData["num"] = Nums[startIdx]
					case ratCounter.Get() >= 2:
						moveData["keyword"] = "lost"
						startIdx = 4
					}
					ratCounter.Inc(Nums[startIdx])
					self.DealDamage(enemy, Nums[startIdx+1], nil)
					return mannymon.TurnDone, t, moveData
				}
			}),
		mannymon.NewMove(
			"Mutated Rat Goons",
			strings.Join([]string{
				"Depending on rat count:",
				"＀＀• 0 rats: Increase rats by %d, and deal %d damage, and get an extra turn",
				"＀＀• 1 rat: Decrease rats by %d, and deal %d damage",
				"＀＀• 2 or more rats: Lose all rats, but deal %d + ( %d * rats ) damage",
			}, "\n"),
			"", // look at the package vars to find the tempates used
			[]int{2, 1, -1, 5, 2, 3}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					var dmg int
					var ratChange int
					var turnStatus = mannymon.TurnDone
					t = nil
					data := map[string]interface{}{}
					switch {
					case ratCounter.Get() == 0:
						ratChange = Nums[0]
						dmg = Nums[1]
						data["ratNum"] = ratChange
						turnStatus = mannymon.TurnExtra
						t = mutatedRatGoonsT1
					case ratCounter.Get() == 1:
						ratChange = Nums[2]
						data["ratNum"] = -ratChange
						dmg = Nums[3]
						t = mutatedRatGoonsT2
					case ratCounter.Get() >= 2:
						dmg = Nums[4] + (ratCounter.Get() * Nums[5])
						ratCounter.Reset()
						t = mutatedRatGoonsT3
					}
					ratCounter.Inc(ratChange)
					self.DealDamage(enemy, dmg, nil)
					return turnStatus, t, data
				}
			}), mannymon.NewMove(
			"Rat Revolution",
			strings.Join([]string{
				"Depending on rat count:",
				"＀＀• 0 rats: Inflict %d damage to self",
				"＀＀• 1 rat: Deal %d damage",
				"＀＀• 2 <= rats <= 5: Deal damage equal to enemy's current HP",
				"＀＀• 6 or more rats: Inflict %d damage to self",
			}, "\n"),
			"{{.self}} hurt himself for {{.selfDmg}} HP",
			[]int{3, 8, 5}, func(Nums []int, t mannymon.Template) mannymon.MoveFunc {
				return func(self, enemy mannymon.Monster, opts *mannymon.Options) (mannymon.TurnStatus, mannymon.Template, mannymon.MoveData) {
					data := map[string]interface{}{}
					switch {
					case ratCounter.Get() == 0:
						self.TakeDamage(self, Nums[0], opts)
						data["selfDmg"] = Nums[0]
					case ratCounter.Get() == 1:
						self.DealDamage(enemy, Nums[1], opts)
						t = ratRevT
					case ratCounter.Get() >= 2 && ratCounter.Get() <= 5:
						self.DealDamage(enemy, enemy.GetHP(), opts)
						t = ratRevT
					case ratCounter.Get() >= 6:
						self.TakeDamage(self, Nums[2], opts)
						data["selfDmg"] = Nums[2]
					}
					return mannymon.TurnDone, t, data
				}
			}),
	}
}

func (m *Ratking) GetImage() []byte {
	return nil
}

func init() {
	mannymon.AddMon("ratking", New)
}
