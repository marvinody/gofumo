package cowsay

import (
	"database/sql"
	"fmt"
	"os/exec"
	"strings"

	"bitbucket.org/marvinody/gofumo"
	"github.com/bwmarrin/discordgo"
)

type cowsay struct {
}

func (cs *cowsay) Name() string {
	return "cowsay"
}

func (cs *cowsay) Description() string {
	return "Get cow to say stuff"
}

func (cs *cowsay) Help(userPerms int) *discordgo.MessageEmbed {
	return gofumo.MakeEmbedForPlugin(cs, userPerms)
}

func (cs *cowsay) MinimumPermissions() int {
	return gofumo.CalculateMinimumPermissions(cs.Commands())
}

func (cs *cowsay) Commands() []gofumo.Command {
	return []gofumo.Command{
		gofumo.NewMultiSubCommand(
			[]gofumo.CommandSyntax{
				gofumo.CommandSyntax{"cowsay", "<phrase>", "cow says phrase"},
				gofumo.CommandSyntax{"cowthink", "<phrase>", "cow thinks phrase"},
			}, gofumo.Prefix, false, discordgo.PermissionSendMessages, cs.CowsayHandler()),
	}
}

func (cs *cowsay) CowsayHandler() gofumo.EventHandlerGenerator {
	return func(botChecker gofumo.BotIDChecker, comChecker gofumo.CommandChecker, permChecker gofumo.PermissionChecker) gofumo.EventHandler {
		return func(s *discordgo.Session, m *discordgo.MessageCreate) {
			if botChecker(s, m) {
				return
			}
			if !comChecker(cs, m) {
				return
			}
			if !permChecker(s, m) {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			split := strings.SplitN(m.ContentWithMentionsReplaced(), " ", 2)
			if len(split) < 2 {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			// this is 100% definitely cowsay or cowthink because of comchecker
			commandText := split[0][1:]
			text := split[1]
			cmd := exec.Command(commandText, text)
			out, err := cmd.Output()
			if err != nil {
				return
			}
			outText := string(out)
			if strings.HasPrefix(outText, "Cow files in") {
				return
			}
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("```\n%s```", outText))
		}
	}
}

func (cs *cowsay) Load(db *sql.DB, keys map[string]string) {

}

func (cs *cowsay) Save(db *sql.DB) {

}

func init() {
	gofumo.RegisterPlugin(&cowsay{})
}
