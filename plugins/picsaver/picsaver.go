package picsaver

import (
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marvinody/gofumo"
	"bitbucket.org/marvinody/gofumo/utils"
	"github.com/bwmarrin/discordgo"
	"github.com/gabriel-vasile/mimetype"
)

var allowedMimes = map[string]bool{
	"image/jpeg": true,
	"image/png":  true,
	"image/gif":  true,
	"image/webp": true,

	"video/mp4":  true,
	"video/webm": true,
}

var blacklistedTags = map[string]bool{
	"untagged": true,
}

type picsaver struct {
	db    *sql.DB
	embed *discordgo.MessageEmbed

	imgurClientID string
}

func (p *picsaver) Name() string {
	return "picsaver"
}

func (p *picsaver) HasImgurKey() bool {
	return p.imgurClientID != ""
}

func (p *picsaver) Description() string {
	return "simple way to save images and search later"
}

func (p *picsaver) Help(userPerms int) *discordgo.MessageEmbed {
	return gofumo.MakeEmbedForPlugin(p, userPerms)
}

func (p *picsaver) Commands() []gofumo.Command {
	return []gofumo.Command{
		&gofumo.SimpleCommand{
			CommandString:   "save <img url> [tags...]",
			HelpString:      "save the provided image and associates it with any tags (space separated)",
			EventHdlr:       p.SaveImageHandler(),
			NeedsCharPrefix: true,
		},
		gofumo.NewMultiSubCommand(
			[]gofumo.CommandSyntax{
				gofumo.CommandSyntax{"find", "<tag>", "searches for any images associated with given tag and links them"},
				gofumo.CommandSyntax{"find", "untagged", "searches for any images that are untagged"},
				gofumo.CommandSyntax{"find", "#<id>", "searches for an existing image with the provided id"},
			}, gofumo.Prefix, false, discordgo.PermissionSendMessages, p.LoadImageHandler()),
		gofumo.NewMultiSubCommand(
			[]gofumo.CommandSyntax{
				gofumo.CommandSyntax{"delete", "media #<id>", "deletes an image and any tags associated"},
			}, gofumo.Prefix, false, discordgo.PermissionManageMessages, p.DeleteImageHandler()),
		gofumo.NewMultiSubCommand(
			[]gofumo.CommandSyntax{
				gofumo.CommandSyntax{"untag", "#<id> <tags>", "searches for an existing image with the provided id"},
			}, gofumo.Prefix, false, discordgo.PermissionSendMessages, p.UntagImageHandler()),
		&gofumo.SimpleCommand{
			CommandString:   "tag <id> [tags...]",
			HelpString:      "Tags the given id image with the supplied tags (only a-z allowed)",
			EventHdlr:       p.TagImageHandler(),
			NeedsCharPrefix: true,
		},
	}
}

func (p *picsaver) MinimumPermissions() int {
	return gofumo.CalculateMinimumPermissions(p.Commands())
}

// SaveImageHandler will look to see if it has a url followed by tags
// if it has a url, it'll check to see if there's any
func (p *picsaver) SaveImageHandler() func(*discordgo.Session, *discordgo.MessageCreate) {
	urlRegexp := regexp.MustCompile(`https?://.*`) // close enough?
	//splitRegexp := regexp.MustCompile(`\w+|"[\w\s]*"`)
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		split := strings.SplitN(m.Content, " ", 3) // com, url, tags
		if split[0] != fmt.Sprintf("%ssave", gofumo.Prefix) {
			return
		}

		if len(split) < 2 && len(m.Attachments) == 0 {
			return
		}
		// we do this for later so anytime we need to access next thing, it'll always be first
		// now split should be the url, tags or just url or just tags
		split = split[1:]

		var mediaURL string
		if len(m.Attachments) > 0 {
			mediaURL = m.Attachments[0].URL
		} else { // split should have url (maybe malformed)
			mediaURL = split[0]
			if !urlRegexp.MatchString(mediaURL) {
				// couldn't detect url
				s.ChannelMessageSend(m.ChannelID, "Did not any media to attempt to save. Make sure it starts with http or include an attachment!")
				return
			}
			split = split[1:]
		}

		// create temp file to hold the media for now
		tmp, err := ioutil.TempFile("./staging", "")
		// I think this is the right order because defer calls are LIFO
		// so we want to close it and then remove it
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unexpected error getting file, try again later...")
			log.Println(err)
			return
		}
		defer os.Remove(tmp.Name())
		defer tmp.Close()
		// download the actual media
		resp, err := http.Get(mediaURL)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unexpected error getting file, try again later...")
			log.Println(err)
			return
		}
		defer resp.Body.Close()
		// so here, we only copy a certain size
		// if we reach max size, just screw it and don't do anything else
		var maxSize int64 = 8 * 1024 * 1024
		n, err := io.CopyN(tmp, resp.Body, maxSize)
		if n == maxSize {
			// most likely overflow of data, so throw err
			s.ChannelMessageSend(m.ChannelID, "File too large, try something smaller to save")
			return
		}
		// reset seeker cause apparently this is a bug otherwise
		tmp.Seek(0, io.SeekStart)
		// this is important step so we can filter "bad" files
		mime, _, err := mimetype.DetectReader(tmp)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unexpected error processing file, try again later...")
			log.Println(err)
			return
		}
		// is mime in our allowed ones
		if !allowedMimes[mime] {
			s.ChannelMessageSend(m.ChannelID, "Unallowed file type, try another format")
			log.Println(err)
			return
		}
		// make hash
		hasher := sha256.New()
		tmp.Seek(0, io.SeekStart)
		if _, err := io.Copy(hasher, tmp); err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unexpected error processing file, try again later...")
			log.Println(err)
			return
		}
		// encode as just hex string
		hash := hex.EncodeToString(hasher.Sum(nil))
		// check for existing hash in current guild
		rows, err := p.db.Query("SELECT id FROM picsaver_files WHERE guildID=? AND hash=? LIMIT 1", m.GuildID, hash)
		defer rows.Close()
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unexpected error processing file, try again later...")
			log.Println(err)
			return
		}
		// if we have a hit, don't bother
		var id int
		if rows.Next() {
			rows.Scan(&id)
			s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("Media previously uploaded, id:%d", id))
			return
		}
		rows.Close()

		origFilename := getOrigFilename(mediaURL, resp.Header)

		// let's try uploading then. The reason we upload is because imgur would probably be
		// better host and keeps a copy since we're not saving to disk here
		if p.HasImgurKey() {
			uploadedURL, err := utils.TryImgurURLUpload(p.imgurClientID, mediaURL)
			if err != nil {
				log.Println("imgur error: ", err)
				log.Println(origFilename, mime)
				log.Println("Falling back to supplied url or discord url from attachment")
			} else {
				mediaURL = uploadedURL
			}
		}

		// otherwise, not in the system so insert
		res, err := p.db.Exec("INSERT INTO picsaver_files (guildID, url, filename, hash, mimetype) VALUES (?, ?, ?, ?, ?)",
			m.GuildID, mediaURL, origFilename, hash, mime)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Unexpected error processing file, try again later...")
			log.Println(err)
			return
		}
		id64, _ := res.LastInsertId()
		id = int(id64)

		// no tags
		if len(split) == 0 {
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)
			return
		}
		// NOW WE DEAL WITH TAGS YAY...
		tags := strings.Split(strings.Join(split, " "), " ")
		stmt, _ := p.db.Prepare("INSERT INTO picsaver_tags VALUES (?, ?)")
		for _, tag := range tags {
			if formattedTag, ok := isValidTag(tag); ok {
				stmt.Exec(id, formattedTag)
			}
		}
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)
	}
}

func isValidTag(tag string) (string, bool) {
	tag = strings.Trim(tag, " ")
	tag = strings.ToLower(tag)
	removeNonAlphabet := func(r rune) rune {
		if r >= 'a' && r <= 'z' {
			return r
		}
		return -1
	}
	tag = strings.Map(removeNonAlphabet, tag)
	if len(tag) > 32 {
		return "", false
	}
	if blacklistedTags[tag] {
		return "", false
	}
	return tag, true
}

func getOrigFilename(url string, header http.Header) string {
	// check to see if we have a name in content disp.
	if s := header.Get("Content-Disposition"); s != "" {
		_, params, _ := mime.ParseMediaType(s)
		if filename := params["filename"]; filename != "" {
			return filename
		}
	}
	// otherwise fall back to pulling it from the url
	// split on "/" and then pull the last one. should be the url name which is good enough?
	split := strings.Split(url, "/")
	last := split[len(split)-1]
	// now, if there's a query or fragment (rfc3986), remove both of those
	// please note that both statements need to be here. we can't remove only query on
	// because if there's fragment but no query, the name would be wrong
	noFragment := strings.Split(last, "#")[0]
	noQuery := strings.Split(noFragment, "?")[0]
	return noQuery
}

// LoadImageHandler returns a generator for an event which generates a discord handler
func (p *picsaver) LoadImageHandler() gofumo.EventHandlerGenerator {
	// this closure lets us use the picsaver struct's stuff like db
	return func(botChecker gofumo.BotIDChecker, comChecker gofumo.CommandChecker, permChecker gofumo.PermissionChecker) gofumo.EventHandler {
		// this closure lets us use the commandcheck/permcheck utility funcs
		return func(s *discordgo.Session, m *discordgo.MessageCreate) {
			if botChecker(s, m) {
				return
			}
			if !comChecker(p, m) {
				return
			}
			if !permChecker(s, m) {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			split := strings.Split(m.Content, " ")
			if len(split) < 2 { // not enough args to do anything
				return
			}
			tags := split[1:]

			// for untagged case
			args := []interface{}{}
			// query to look for entries with no matching entries in tag table
			query := `
		    SELECT
		      id, url, filename, mimetype
		      FROM picsaver_files f
		      WHERE
		        guildId LIKE ?
		       AND NOT EXISTS (
		         SELECT 1 FROM picsaver_tags t
		         WHERE f.id = t.fileId
		       )
		       LIMIT 10;
		    `
			// 2 args and if first thing is numeric or if is #numeric
			isFirstArgNumeric := len(split) > 1 && (isNumeric(split[1]) || (split[1][0] == '#' && isNumeric(split[1][1:])))
			lookingForTaggedMedia := !(len(split) == 2 && (split[1] == "untagged" || isFirstArgNumeric))
			lookingForSoleMedia := isFirstArgNumeric && len(split) == 2

			// general tag query. ie ham burger
			if lookingForTaggedMedia {
				// essentially the idea to do have a bunch of mini queries and then intersect
				// this probably isn't the most efficient, but I can't seem to get a query that works the way I want
				// ie. matches only the search terms and nothing extra. ham burger should find ham burger but not burger or ham.
				queryStmts := []string{}
				for range tags {
					queryStmts = append(queryStmts, "SELECT fileID FROM picsaver_tags WHERE tag LIKE ?")
				}
				query = fmt.Sprintf(`
		      SELECT
		        id, url, filename, mimetype
		      FROM (%s) AS tagged
		      INNER JOIN picsaver_files
		        ON tagged.fileID = picsaver_files.id
		      WHERE
		        guildId LIKE ?
		      LIMIT 20
		      `,
					strings.Join(queryStmts, " INTERSECT "),
				)
				// convert to interface cause that's silly
				args = make([]interface{}, len(tags), len(tags)+1)
				for i, v := range tags {
					args[i] = v
				}
			}
			if lookingForSoleMedia {
				query = `
		      SELECT
		        id, url, filename, mimetype
		        FROM picsaver_files f
		        WHERE
		          id = ?
		        AND guildId LIKE ?
		         LIMIT 10;
		      `
				// id has to be num tho, so let's try parsing
				numStr := split[1]
				if !isNumeric(split[1]) {
					numStr = split[1][1:]
				}

				num, err := strconv.Atoi(numStr)
				if err != nil {
					s.ChannelMessageSend(m.ChannelID, "Expecting a number following '#', like '#4'")
					return
				}

				args = append(args, num)
			}
			// all queries have guildid as the last arg
			args = append(args, m.GuildID)
			// actually run the query and get the results
			results, err := getMediaMatchingQuery(p.db, query, args)
			if err != nil {
				fmt.Println(err)
				s.ChannelMessageSend(m.ChannelID, "Unexpected searching for tags, try again later...")
				return
			}
			// if we're looking for untagged stuff, then this step doesn't make sense
			if lookingForTaggedMedia || lookingForSoleMedia {
				tagQuery, _ := p.db.Prepare("SELECT tag FROM picsaver_tags WHERE fileID = ? LIMIT 20")
				for idx, mediaResult := range results {
					rows, _ := tagQuery.Query(mediaResult.id)
					tags := []string{}
					for rows.Next() {
						var tag string
						rows.Scan(&tag)
						tags = append(tags, strings.Title(tag))
					}
					rows.Close()
					results[idx].tags = tags
				}
			}
			// no results, just put cross on msg
			if len(results) == 0 {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			// paginate any results we have
			duration := time.Minute * 5
			resultPaginator(s, results, m.ChannelID, duration)
		}
	}

}

// runs the query with args and returns media results
func getMediaMatchingQuery(db *sql.DB, query string, args []interface{}) ([]mediaResult, error) {
	rows, err := db.Query(query, args...)
	defer rows.Close()

	if err != nil {
		log.Println(err)
		return nil, err
	}
	results := []mediaResult{}

	for rows.Next() {
		var (
			id            int
			url, filename string
			mimetype      string
		)
		rows.Scan(&id, &url, &filename, &mimetype)
		results = append(results, mediaResult{
			id:       id,
			url:      url,
			filename: filename,
			mimetype: mimetype,
		})
	}
	return results, nil
}

func makePageEmbed(idx int, results []mediaResult) *discordgo.MessageEmbed {
	embed := &discordgo.MessageEmbed{
		Title:       fmt.Sprintf("%s (#%d)", results[idx].filename, results[idx].id),
		Description: strings.Join(results[idx].tags, ", "),
		URL:         results[idx].url,
		Author:      gofumo.EmbedAuthor,
		Color:       gofumo.EmbedColor,
		Footer: &discordgo.MessageEmbedFooter{
			Text: fmt.Sprintf("%d / %d", idx+1, len(results)),
		},
	}
	if strings.Contains(results[idx].mimetype, "video") {
		embed.Video = &discordgo.MessageEmbedVideo{
			URL: results[idx].url,
		}
	} else if strings.Contains(results[idx].mimetype, "image") {
		embed.Image = &discordgo.MessageEmbedImage{
			URL: results[idx].url,
		}
	}
	return embed
}

// Really unreadable in the sense of arguments going in
// it takes in a bunch of "state" stuff
func resultPaginator(s *discordgo.Session, results []mediaResult, chID string, keepAlive time.Duration) {
	idx := 0
	msg, _ := s.ChannelMessageSendEmbed(chID, makePageEmbed(idx, results))

	// only one result, don't bother to paginate anything!
	if len(results) == 1 {
		return
	}
	s.MessageReactionAdd(chID, msg.ID, gofumo.EmojiArrowLeft)
	s.MessageReactionAdd(chID, msg.ID, gofumo.EmojiArrowRight)
	// loop indefinitely until time runs out
	utils.WaitUntilReact(s, msg.ID, func(m *discordgo.MessageReactionAdd) bool {
		// update index and embed
		if m.Emoji.Name == gofumo.EmojiArrowRight {
			idx = utils.Mod(idx+1, len(results))
			embed := makePageEmbed(idx, results)
			s.ChannelMessageEditEmbed(m.ChannelID, m.MessageID, embed)
		} else if m.Emoji.Name == gofumo.EmojiArrowLeft {
			idx = utils.Mod(idx-1, len(results))
			embed := makePageEmbed(idx, results)
			s.ChannelMessageEditEmbed(m.ChannelID, m.MessageID, embed)
		}
		// keep looping until dead time
		return false
	}, time.Now().Add(keepAlive))
}

func (p *picsaver) TagImageHandler() func(*discordgo.Session, *discordgo.MessageCreate) {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		split := strings.Split(m.Content, " ")
		if split[0] != fmt.Sprintf("%stag", gofumo.Prefix) {
			return
		}
		if len(split) < 3 {
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
			return
		}
		numStr := split[1]
		if !isNumeric(split[1]) {
			numStr = split[1][1:]
		}

		id, err := strconv.Atoi(numStr)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Expecting a number following '#', like '#4'")
			return
		}

		res, _ := p.db.Query("SELECT * FROM picsaver_files WHERE id=? AND guildID LIKE ?", id, m.GuildID)
		if !res.Next() {
			s.ChannelMessageSend(m.ChannelID, "ID doesn't seem to exist. Are you sure it's correct?")
			return
		}
		res.Close()

		tags := strings.Split(split[2], " ")
		stmt, _ := p.db.Prepare("INSERT INTO picsaver_tags VALUES (?, ?)")
		for _, tag := range tags {
			// don't really care about errs here because only error should be dupes
			if formattedTag, ok := isValidTag(tag); ok {
				stmt.Exec(id, formattedTag)
			}
		}
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)

	}
}

// UntagImageHandler returns a generator for an event which generates a discord handler
func (p *picsaver) UntagImageHandler() gofumo.EventHandlerGenerator {
	// this closure lets us use the picsaver struct's stuff like db
	return func(botChecker gofumo.BotIDChecker, comChecker gofumo.CommandChecker, permChecker gofumo.PermissionChecker) gofumo.EventHandler {
		// this closure lets us use the commandcheck/permcheck utility funcs
		return func(s *discordgo.Session, m *discordgo.MessageCreate) {
			if botChecker(s, m) {
				return
			}
			if !comChecker(p, m) {
				return
			}
			if !permChecker(s, m) {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			split := strings.Split(m.Content, " ")
			// split should be min length 3, "untag", "<id>", "<tags>"
			if len(split) < 3 {
				return
			}
			numStr := split[1]
			if !isNumeric(split[1]) {
				numStr = split[1][1:]
			}

			num, err := strconv.Atoi(numStr)
			if err != nil {
				s.ChannelMessageSend(m.ChannelID, "Expecting a number following '#', like '#4'")
				return
			}
			row, err := p.db.Query(`SELECT id from picsaver_files where id=? AND guildID LIKE ?`, num, m.GuildID)
			if err != nil {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				log.Println(err)
				return
			}
			defer row.Close()
			if row.Next() {
				row.Scan(&num)
			} else {
				s.ChannelMessageSend(m.ChannelID, "The requested id doesn't seem to exist for this server")
				return
			}
			row.Close()
			stmt, err := p.db.Prepare(`DELETE FROM picsaver_tags WHERE fileID=? AND tag LIKE ?`)
			if err != nil {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				log.Println(err)
				return
			}
			defer stmt.Close()
			tags := split[2:]
			for idx := range tags {
				tag := tags[idx]
				stmt.Exec(num, tag)
			}
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)
		}
	}
}

// DeleteImageHandler returns a generator for an event which generates a discord handler
func (p *picsaver) DeleteImageHandler() gofumo.EventHandlerGenerator {
	// this closure lets us use the picsaver struct's stuff like db
	return func(botChecker gofumo.BotIDChecker, comChecker gofumo.CommandChecker, permChecker gofumo.PermissionChecker) gofumo.EventHandler {
		// this closure lets us use the commandcheck/permcheck utility funcs
		return func(s *discordgo.Session, m *discordgo.MessageCreate) {
			if botChecker(s, m) {
				return
			}
			if !comChecker(p, m) {
				return
			}
			if !permChecker(s, m) {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				return
			}
			split := strings.Split(m.Content, " ")
			// split should be len 3, delete, media, 12
			if len(split) != 3 || split[1] != "media" {
				return
			}
			numStr := split[2]
			if !isNumeric(split[2]) {
				numStr = split[2][1:]
			}

			num, err := strconv.Atoi(numStr)
			if err != nil {
				s.ChannelMessageSend(m.ChannelID, "Expecting a number following '#', like '#4'")
				return
			}
			row, err := p.db.Query(`SELECT id from picsaver_files where id=? AND guildID LIKE ?`, num, m.GuildID)
			if err != nil {
				s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
				log.Println(err)
				return
			}
			defer row.Close()
			if row.Next() {
				row.Scan(&num)
			} else {
				s.ChannelMessageSend(m.ChannelID, "The requested id doesn't seem to exist for this server")
				return
			}
			row.Close()
			p.db.Exec("DELETE FROM picsaver_files WHERE id = ? AND guildID LIKE ?", num, m.GuildID)
			p.db.Exec("DELETE FROM picsaver_tags WHERE fileId", num)

		}
	}
}

type mediaResult struct {
	id       int
	url      string
	filename string
	mimetype string
	tags     []string
}

func isNumeric(s string) bool {
	_, err := strconv.Atoi(s)
	return err == nil
}

func (p *picsaver) Load(db *sql.DB, keys map[string]string) {

	// we'll need this for later
	p.db = db
	p.imgurClientID = keys["imgur"]

	// incase we don't have the table, create it
	stmt := `
	CREATE TABLE IF NOT EXISTS picsaver_files ( id INTEGER PRIMARY KEY UNIQUE,
    guildID TEXT, filename TEXT, url TEXT, hash TEXT, mimetype TEXT );
  CREATE TABLE IF NOT EXISTS picsaver_tags ( fileID INTEGER, tag TEXT );
  CREATE INDEX IF NOT EXISTS guildIndex ON picsaver_files ( guildID );
  CREATE UNIQUE INDEX IF NOT EXISTS hashIndex ON picsaver_files ( hash, guildID );
  CREATE INDEX IF NOT EXISTS tagIndex ON picsaver_tags ( tag );
  CREATE UNIQUE INDEX IF NOT EXISTS fileTag ON picsaver_tags ( fileID, tag );
  CREATE INDEX IF NOT EXISTS fileIDIndex ON picsaver_tags ( fileID );
	`

	os.Mkdir("staging", 0700)

	_, err := db.Exec(stmt)
	if err != nil {
		log.Fatal(err)
	}
}

func (p *picsaver) Save(db *sql.DB) {

}

func init() {
	ps := &picsaver{}

	gofumo.RegisterPlugin(ps)
}
