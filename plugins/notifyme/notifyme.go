package notifyme

import (
	"database/sql"
	"fmt"
	"log"
	"math"
	"regexp"
	"strings"
	"sync"
	"time"

	"bitbucket.org/marvinody/gofumo"
	"bitbucket.org/marvinody/gofumo/utils"
	"github.com/bwmarrin/discordgo"
)

const (
	resultsPerPage = 6
)

type notifyme struct {
	cnt  int
	help discordgo.MessageEmbed

	db *sql.DB
	// guildId  channelID   text    userIDs
	//records lockMap
	// userID    guildID    channelID  texts
	//userRecords lockMap
}

type lockMap struct {
	sync.RWMutex
	m map[string]map[string]map[string]map[string]bool
}

func (n *notifyme) Name() string {
	return "notifyme"
}

func (n *notifyme) Description() string {
	return "Will let you know when stuff is said somewhere"
}

func (n *notifyme) Help(userPerms int) *discordgo.MessageEmbed {
	return gofumo.MakeEmbedForPlugin(n, userPerms)
}

func (n *notifyme) MinimumPermissions() int {
	return gofumo.CalculateMinimumPermissions(n.Commands())
}

func (n *notifyme) Commands() []gofumo.Command {
	return []gofumo.Command{
		&gofumo.SimpleCommand{
			CommandString:     "of <text> in #<channel>",
			HelpString:        "Lets you know when some certain text is posted in wanted channel",
			EventHdlr:         n.OfHandlerCreateRemember(),
			NeedsModulePrefix: true,
		},
		&gofumo.SimpleCommand{
			CommandString:     "delete",
			HelpString:        "Allows you delete notify events for any channel in current server",
			EventHdlr:         n.OfHandlerDeleteRemember(),
			NeedsModulePrefix: true,
		},
		&gofumo.SimpleCommand{
			CommandString: "This should never appear in help",
			EventHdlr:     n.OfEmbedHandlerCheckRemember(),
			Permissions:   -1,
		},
	}
}

func (n *notifyme) OfEmbedHandlerCheckRemember() func(*discordgo.Session, *discordgo.MessageCreate) {
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		guildID, channelID, msgText := m.GuildID, m.ChannelID, m.Content
		lowerText := strings.ToLower(msgText)
		if s.State.User.ID == m.Author.ID {
			return
		}
		if strings.HasPrefix(lowerText, "notifyme") {
			return
		}
		results := n.findMatchingTexts(guildID, channelID, msgText)
		if len(results) == 0 {
			return
		}
		guild, _ := s.Guild(guildID)
		channel, _ := s.Channel(channelID)
		for _, result := range results {
			ch, err := s.UserChannelCreate(result.userID)
			if err != nil {
				log.Println(err)
			}
			s.ChannelMessageSend(ch.ID, fmt.Sprintf("(%s) mentioned on '%s', channel: '%s'!", result.text, guild.Name, channel.Name))
		}
	}
}

func (n *notifyme) OfHandlerCreateRemember() func(*discordgo.Session, *discordgo.MessageCreate) {
	splitRegexp := regexp.MustCompile(`(?i)notifyme of (?P<text>.*) in <#(?P<channel>\d+)>`)
	preText := "notifyme"
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if len(m.Content) < len(preText) {
			return
		}
		prefixText := strings.ToLower(m.Content[:len(preText)])
		if prefixText != preText {
			return
		}
		match := splitRegexp.FindStringSubmatch(m.Content)
		if match == nil {
			return
		}
		text := strings.ToLower(match[1])
		channel := match[2]
		perms, err := s.UserChannelPermissions(m.Author.ID, channel)
		if err != nil {
			log.Println(err)
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
			return
		}
		if !gofumo.ContainsPermission(perms, discordgo.PermissionReadMessages) {
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
			return
		}
		err = n.addRecord(m.GuildID, channel, text, m.Author.ID)
		if err != nil {
			log.Println(err)
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCrossMark)
			return
		}
		s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)
	}
}

func (n *notifyme) addRecord(guildID, channelID, text, userID string) error {
	stmt := `
	INSERT INTO notifyme_reminders
		(guildID, channelID, userID, text)
	VALUES
		(?, ?, ?, ?)
	`
	_, err := n.db.Exec(stmt, guildID, channelID, userID, text)
	return err
}

// will delete the record and update the result arr
func (n *notifyme) deleteRecord(results []*notifyResult, page, idx int) ([]*notifyResult, error) {
	realIdx := page*resultsPerPage + idx
	toDelete := results[realIdx]
	stmt := `
	DELETE FROM
		notifyme_reminders
	WHERE
		guildID LIKE ? AND
		channelID LIKE ? AND
		userID LIKE ? AND
		text LIKE ?
	`
	_, err := n.db.Exec(stmt, toDelete.guildID, toDelete.channelID, toDelete.userID, toDelete.text)
	if err != nil {
		log.Println(err)
		return results, err
	}
	return splice(realIdx, results), nil
}

func (n *notifyme) OfHandlerDeleteRemember() func(*discordgo.Session, *discordgo.MessageCreate) {
	command := "notifyme delete"
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if len(m.Content) != len(command) {
			return
		}
		lowerText := strings.ToLower(m.Content)
		if lowerText != command {
			return
		}
		results := n.getAllInGuild(m.Author.ID, m.GuildID)
		if len(results) == 1 {
			s.MessageReactionAdd(m.ChannelID, m.ID, gofumo.EmojiCheckMark)
			n.deleteRecord(results, 0, 0)
			return
		}

		// get total amt.
		pages := int(math.Ceil(float64(len(results)) / float64(resultsPerPage)))
		page := 0
		// initial embed
		msg, _ := s.ChannelMessageSendEmbed(m.ChannelID, makeEmbed(s, page, pages, results))
		// do all the reacts
		if pages > 1 { // if 1 page, don't need arrows
			s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowLeft)
		}
		// if less than 6, do <6, otherwise 6
		for i := 1; i <= clamp(len(results), resultsPerPage); i++ {
			s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.NumberToEmoji[i])
		}
		if pages > 1 { // if 1 page, don't need arrows
			s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiArrowRight)
		}
		// user can stop it early if they want to
		s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiCrossMark)
		origUserID := m.Author.ID
		// loop, waiting for user
		utils.WaitUntilReact(s, msg.ID, func(m *discordgo.MessageReactionAdd) bool {
			// only orig user can do stuff
			if m.UserID != origUserID {
				return false
			}
			emoji := m.Emoji.APIName()
			// this is the only time when we want to actually stop
			if emoji == gofumo.EmojiCrossMark {
				s.MessageReactionAdd(m.ChannelID, msg.ID, gofumo.EmojiCheckMark)
				return true
			}
			rerender := false
			// if arrows, just change the page
			if emoji == gofumo.EmojiArrowLeft {
				page, rerender = utils.Mod(page-1, pages), true
			} else if emoji == gofumo.EmojiArrowRight {
				page, rerender = utils.Mod(page+1, pages), true
			} else if num, ok := gofumo.EmojiToNumber[emoji]; ok { // if user gave a number, let's delete it
				// if number we're not using, do nothing
				if num == 0 || num > clamp(len(results), resultsPerPage) {
					return false
				}
				// finally delete it
				results, _ := n.deleteRecord(results, page, num-1)
				rerender = true
				// if we delete a full page, just reset back to 1
				newPages := int(math.Ceil(float64(len(results)) / float64(resultsPerPage)))
				if newPages < pages {
					page = utils.Mod(page-1, pages)
					pages = newPages
				}
			}
			if rerender {
				s.ChannelMessageEditEmbed(m.ChannelID, msg.ID, makeEmbed(s, page, pages, results))

			}
			// continue while we have results
			return len(results) == 0

		}, time.Now().Add(time.Second*60))
	}
}

func makeEmbed(s *discordgo.Session, page, totalPages int, results []*notifyResult) *discordgo.MessageEmbed {
	startIdx := page * resultsPerPage
	endIdx := startIdx + resultsPerPage
	endIdx = clamp(endIdx, len(results))
	curResults := results[startIdx:endIdx]
	embed := utils.NewEmbed().
		SetColor(gofumo.EmbedColor).
		SetTitle("Delete a Reminder").
		SetDesc("Emote a number to select\nUse arrows to change pages\nHit the X when you're done")
	for idx, result := range curResults {
		n := idx + 1
		ch, _ := s.Channel(result.channelID)
		embed.AddField(fmt.Sprintf("%d: #**%s**", n, ch.Name), result.text, false)
	}
	embed.SetFooter("", fmt.Sprintf("%d / %d", page+1, totalPages))
	return embed.Get()
}
func clamp(a, b int) int {
	if a < b {
		return a
	}
	return b
}
func (n *notifyme) getAllInGuild(userID, guildID string) []*notifyResult {
	query := `SELECT channelID, text
		FROM notifyme_reminders
		WHERE
			guildID LIKE ? AND
			userID LIKE ?;`
	rows, _ := n.db.Query(query, guildID, userID)
	var (
		channelID string
		text      string
	)
	results := []*notifyResult{}
	for rows.Next() {
		rows.Scan(&channelID, &text)
		results = append(results, &notifyResult{
			guildID:   guildID,
			channelID: channelID,
			userID:    userID,
			text:      text,
		})
	}
	return results
}

func (n *notifyme) findMatchingTexts(guildID, channelID, text string) []*notifyResult {
	query := `SELECT userID, text
		FROM notifyme_reminders
		WHERE
			guildID LIKE ? AND
			channelID LIKE ? AND
			? LIKE "%"||notifyme_reminders.text||"%" ;`
	rows, _ := n.db.Query(query, guildID, channelID, text)
	var (
		userID     string
		wantedText string
	)
	results := []*notifyResult{}
	for rows.Next() {
		rows.Scan(&userID, &wantedText)
		results = append(results, &notifyResult{
			guildID:   guildID,
			channelID: channelID,
			userID:    userID,
			text:      wantedText,
		})
	}
	return results
}

type notifyResult struct {
	guildID   string
	channelID string
	userID    string
	text      string
}

func splice(i int, l []*notifyResult) []*notifyResult {
	new := make([]*notifyResult, len(l)-1, cap(l))
	copy(new[:i], l[:i])
	copy(new[i:], l[i+1:])
	return new
}

func (n *notifyme) Load(db *sql.DB, keys map[string]string) {
	n.db = db
	// incase we don't have the table, create it
	stmt := `
	CREATE TABLE IF NOT EXISTS notifyme_reminders ( id INTEGER PRIMARY KEY UNIQUE,
    guildID TEXT, channelID TEXT, userID TEXT, text TEXT );
  CREATE INDEX IF NOT EXISTS guildUserIndex ON notifyme_reminders ( guildID, userID );
  CREATE INDEX IF NOT EXISTS guildChannelIndex ON notifyme_reminders ( guildID, channelID );
  CREATE UNIQUE INDEX IF NOT EXISTS entryIndex ON notifyme_reminders ( guildID, channelID, userID, text );
	`
	_, err := db.Exec(stmt)
	if err != nil {
		log.Fatal(err)
	}
}

func (n *notifyme) Save(db *sql.DB) {
}

func init() {
	gofumo.RegisterPlugin(&notifyme{})

}
