package pingpong

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marvinody/gofumo"
	"bitbucket.org/marvinody/gofumo/utils"
	"github.com/bwmarrin/discordgo"
)

type pingpong struct {
	cnt  int
	help discordgo.MessageEmbed
}

func (p *pingpong) Name() string {
	return "pingpong"
}

func (p *pingpong) Description() string {
	return "silly ping pong thing"
}

func (p *pingpong) Help(userPerms int) *discordgo.MessageEmbed {
	return gofumo.MakeEmbedForPlugin(p, userPerms)
}

func (p *pingpong) MinimumPermissions() int {
	return gofumo.CalculateMinimumPermissions(p.Commands())
}

func (p *pingpong) Commands() []gofumo.Command {
	return []gofumo.Command{
		&gofumo.SimpleCommand{
			CommandString: "ping",
			HelpString:    "replies with pong",
			EventHdlr:     messageCreate, // this one doesn't require any state so we can handle it outside
			// and not need to use closure of the struct to manage any data
			// alternatively, one could use globals and do it that way but I'm trying to make it nice
		},
		&gofumo.SimpleCommand{
			CommandString: "inc [num]",
			HelpString:    "increases an internal counter by the specified amount (default 1)",
			EventHdlr: func(s *discordgo.Session, m *discordgo.MessageCreate) {
				if m.Author.ID == s.State.User.ID {
					return
				}
				split := strings.SplitN(m.Content, " ", 2)
				if split[0] == "inc" {
					n := 1
					if len(split) > 1 {
						newN, err := strconv.Atoi(split[1])
						if err != nil {
							s.MessageReactionAdd(m.ChannelID, m.ID, "❎")
							return
						}
						n = newN
					}
					p.cnt += n
					s.MessageReactionAdd(m.ChannelID, m.ID, "✅")
				}
			},
		},
		&gofumo.SimpleCommand{
			CommandString: "dec [num]",
			HelpString:    "decreases an internal counter by the specified amount (default 1)",
			EventHdlr: func(s *discordgo.Session, m *discordgo.MessageCreate) {
				if m.Author.ID == s.State.User.ID {
					return
				}
				split := strings.SplitN(m.Content, " ", 2)
				if split[0] == "dec" {
					n := 1
					if len(split) > 1 {
						newN, err := strconv.Atoi(split[1])
						if err != nil {
							s.MessageReactionAdd(m.ChannelID, m.ID, "❎")
							return
						}
						n = newN
					}
					p.cnt -= n
					s.MessageReactionAdd(m.ChannelID, m.ID, "✅")
				}
			},
		},
		&gofumo.SimpleCommand{
			CommandString: "count",
			HelpString:    "reveals the internal counter",
			EventHdlr: func(s *discordgo.Session, m *discordgo.MessageCreate) {
				if m.Author.ID == s.State.User.ID {
					return
				}
				if m.Content == "count" {
					s.ChannelMessageSend(m.ChannelID, fmt.Sprintf("%d", p.cnt))
				}
			},
		},
		&gofumo.SimpleCommand{
			CommandString: "!emotecode",
			HelpString:    "React with an emoji and I'll pm you the code",
			EventHdlr: func(s *discordgo.Session, m *discordgo.MessageCreate) {
				if m.Author.ID == s.State.User.ID {
					return
				}
				if m.Content == "!emotecode" {
					utils.WaitUntilReact(s, m.ID, utils.ReactHandler(func(e *discordgo.MessageReactionAdd) bool {
						if m.Author.ID != e.UserID {
							return false
						}
						s.ChannelMessageSend(m.ChannelID, e.Emoji.APIName())
						return true
					}), time.Now().Add(time.Minute))
				}
			},
		},
	}
}

func (p *pingpong) Load(db *sql.DB, keys map[string]string) {
	// incase we don't have the table, create it
	stmt := `
	CREATE TABLE IF NOT EXISTS pingpong(count integer);
	`

	_, err := db.Exec(stmt)
	if err != nil {
		log.Fatal(err)
	}
	// pull current data unless we don't have any, then populate it
	rows, err := db.Query("SELECT count FROM pingpong LIMIT 1")
	if err != nil {
		log.Fatal(err)
	}
	if rows.Next() {
		err := rows.Scan(&p.cnt)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		stmt := `INSERT INTO pingpong VALUES (0)`
		_, err := db.Exec(stmt)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func (p *pingpong) Save(db *sql.DB) {
	stmtS := `
	UPDATE pingpong SET count = ?
	`
	stmt, err := db.Prepare(stmtS)
	if err != nil {
		log.Println(err)
	}
	_, err = stmt.Exec(p.cnt)
	if err != nil {
		log.Println(err)
	}
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	// If the message is "ping" reply with "Pong!"
	if m.Content == "ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}
}

func init() {
	gofumo.RegisterPlugin(&pingpong{})
}
