package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"bitbucket.org/marvinody/gofumo"
	_ "bitbucket.org/marvinody/gofumo/plugins/cowsay"
	_ "bitbucket.org/marvinody/gofumo/plugins/mannymon/game"
	_ "bitbucket.org/marvinody/gofumo/plugins/notifyme"
	_ "bitbucket.org/marvinody/gofumo/plugins/picsaver"
	_ "bitbucket.org/marvinody/gofumo/plugins/pingpong"
)

// Variables used for command line parameters
var (
	Token string

	ImgurClientID string
)

func init() {
	Token = os.Getenv("BOT_TOKEN")
	ImgurClientID = os.Getenv("IMGUR_API_KEY")
}

func main() {

	keys := map[string]string{
		"imgur": ImgurClientID,
	}

	// Create a new Discord session using the provided bot token.
	gf, err := gofumo.New(Token, keys)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// Open a websocket connection to Discord and begin listening.
	err = gf.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	log.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	gf.Close()
}
