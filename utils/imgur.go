package utils

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
)

type ImgurDataResponse struct {
	ID    string
	Link  string
	Error string
}
type ImgurBaseResponse struct {
	Data    ImgurDataResponse
	Success bool
	Status  int
}

func TryImgurMediaUpload(key string, media []byte) (string, error) {
	client := &http.Client{}
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fw, _ := w.CreateFormField("image")
	fw.Write(media)
	w.Close()
	req, _ := http.NewRequest(
		http.MethodPost,
		"https://api.imgur.com/3/image",
		&b,
	)
	req.Header.Set("Authorization", "Client-ID "+key)
	req.Header.Set("Content-Type", w.FormDataContentType())
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	imgurUpload := &ImgurBaseResponse{}
	err = json.Unmarshal(body, imgurUpload)
	if err != nil {
		return "", err
	}
	if !imgurUpload.Success {
		return "", fmt.Errorf(
			"%d: %s", imgurUpload.Status, imgurUpload.Data.Error,
		)
	}
	return imgurUpload.Data.Link, nil
}

func TryImgurURLUpload(key, mediaURL string) (string, error) {
	client := &http.Client{}
	req, _ := http.NewRequest(
		http.MethodPost,
		"https://api.imgur.com/3/image",
		strings.NewReader(url.Values{
			"image": []string{mediaURL},
		}.Encode()))
	req.Header.Set("Authorization", "Client-ID "+key)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	imgurUpload := &ImgurBaseResponse{}
	err = json.Unmarshal(body, imgurUpload)
	if err != nil {
		return "", err
	}
	if !imgurUpload.Success {
		return "", fmt.Errorf(
			"%d: %s", imgurUpload.Status, imgurUpload.Data.Error,
		)
	}
	return imgurUpload.Data.Link, nil
}
