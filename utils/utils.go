package utils

import (
	"sync"
	"time"

	"github.com/bwmarrin/discordgo"
)

// LockMapArrString is like LockMap but has string arr instead of just a string
type LockMapArrString struct {
	m map[string][]string
	*sync.RWMutex
}

// NewLockMapArrString creates new LockMapArrString
func NewLockMapArrString() *LockMapArrString {
	return &LockMapArrString{
		make(map[string][]string),
		&sync.RWMutex{},
	}
}

func (lock *LockMapArrString) Get(key string) []string {
	lock.RLock()
	defer lock.RUnlock()
	val := lock.m[key]
	return val
}

func (lock *LockMapArrString) Set(key string, val []string) {
	lock.Lock()
	defer lock.Unlock()
	lock.m[key] = val
}

// LockMapString is LockMapString implementation with a string key and bool value
type LockMapString struct {
	m map[string]string
	*sync.RWMutex
}

// NewLockMap creates new LockMap
func NewLockMap() *LockMapString {
	return &LockMapString{
		make(map[string]string),
		&sync.RWMutex{},
	}
}

func (lock *LockMapString) Check(key, val string) bool {
	lock.RLock()
	defer lock.RUnlock()
	return lock.m[key] == val
}

func (lock *LockMapString) Set(key, val string) {
	lock.Lock()
	defer lock.Unlock()
	lock.m[key] = val
}

func (lock *LockMapString) Unset(key string) {
	lock.Lock()
	defer lock.Unlock()
	delete(lock.m, key)
}

// DoAfter starts a goroutine that will call f after d time
func DoAfter(d time.Duration, f func()) {
	go func() {
		time.Sleep(d)
		f()
	}()
}

// Mod is the modulus operator but it does not allow negative numbers
// So output will always be [0, b)
// useful for pagination
func Mod(a, b int) int {
	c := a % b
	if c < 0 {
		return c + b
	}
	return c
}

// ReactHandler decides when to stop "looping"
// return true to stop binding
type ReactHandler func(m *discordgo.MessageReactionAdd) bool

// WaitUntilReact will wait lifetime amt. of time until someone reacts on the msgID message and the ReactHandler func returns true
func WaitUntilReact(s *discordgo.Session, msgID string, f ReactHandler, endTime time.Time) {
	s.AddHandlerOnce(func(_ *discordgo.Session, m *discordgo.MessageReactionAdd) {
		// expired
		if time.Now().After(endTime) {
			return
		}
		// wrong msg react or bot reacting initially
		if m.MessageID != msgID || s.State.User.ID == m.UserID {
			WaitUntilReact(s, msgID, f, endTime)
			return
		}
		// if f returns true, we're done
		if f(m) {
			return
		}
		// otherwise, keep binding
		WaitUntilReact(s, msgID, f, endTime)
	})
}

// ReplyHandler decides when to stop "looping"
// return true to stop binding
type ReplyHandler func(m *discordgo.MessageCreate) bool

// WaitUntilReply will wait lifetime amt. of time until someone reply on the guild/channel and the ReplyHandler func returns true
func WaitUntilReply(s *discordgo.Session, guildID, channelID, userID string, f ReplyHandler, endTime time.Time) {
	s.AddHandlerOnce(func(_ *discordgo.Session, m *discordgo.MessageCreate) {
		// expired
		if time.Now().After(endTime) {
			return
		}
		// bot replying to something or another bot saying something
		if s.State.User.ID == m.Author.ID || m.Author.Bot {
			WaitUntilReply(s, guildID, channelID, userID, f, endTime)
			return
		}
		// wrong guild/channel
		if m.GuildID != guildID || m.ChannelID != channelID {
			WaitUntilReply(s, guildID, channelID, userID, f, endTime)
			return
		}
		if m.Author.ID != userID {
			WaitUntilReply(s, guildID, channelID, userID, f, endTime)
			return
		}
		// if f returns true, we're done
		if f(m) {
			return
		}
		// otherwise, keep binding
		WaitUntilReply(s, guildID, channelID, userID, f, endTime)
	})
}

type Embed struct {
	e *discordgo.MessageEmbed
}

func NewEmbed() *Embed {
	return &Embed{&discordgo.MessageEmbed{}}
}

// AddURL sets s to url
func (e *Embed) SetURL(s string) *Embed {
	e.e.URL = s
	return e
}

func (e *Embed) SetAuthor(author, iconURL string) *Embed {
	e.e.Author = &discordgo.MessageEmbedAuthor{
		Name:    author,
		IconURL: iconURL,
	}
	return e
}

// AddTitle sets title to s
func (e *Embed) SetTitle(s string) *Embed {
	e.e.Title = s
	return e
}

// AddDesc sets title to s
func (e *Embed) SetDesc(s string) *Embed {
	e.e.Description = s
	return e
}

func (e *Embed) SetColor(c int) *Embed {
	e.e.Color = c
	return e
}

func (e *Embed) AddField(name, value string, inline bool) *Embed {
	e.e.Fields = append(e.e.Fields, &discordgo.MessageEmbedField{
		Name:   name,
		Value:  value,
		Inline: inline,
	})
	return e
}

func (e *Embed) ResetFields() *Embed {
	e.e.Fields = []*discordgo.MessageEmbedField{}
	return e
}

func (e *Embed) SetFooter(iconURL, text string) *Embed {
	e.e.Footer = &discordgo.MessageEmbedFooter{
		IconURL: iconURL,
		Text:    text,
	}
	return e
}
func (e *Embed) Get() *discordgo.MessageEmbed {
	return e.e
}
