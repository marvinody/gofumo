package gofumo

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	"github.com/bwmarrin/discordgo"

	// load needed driver
	_ "github.com/mattn/go-sqlite3"
)

var (
	plugins []Plugin
	// Prefix that event handlers should look for if needed
	Prefix string

	// EmbedAuthor is a general author struct for embeds to not remake one
	EmbedAuthor *discordgo.MessageEmbedAuthor
	// EmbedColor is the color that's generally used for embeds
	EmbedColor int
)

// EventHandler is a "generic" var that makes sure you know what the handler's sig should be
// It should be a func where the first parameter is a *Session, and the second parameter is a pointer to a struct corresponding to the discordgo event for which you want to listen.
// IE func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {}
type EventHandler interface{}

// Plugin contains the methods that a plugin should have in order to be loaded
// Load will be called before any messages are parsed
// Save will be called before the bot is unloaded gracefully
type Plugin interface {
	Commands() []Command
	Name() string
	// sql connection and map of any keys needed to login to a service
	Load(*sql.DB, map[string]string)
	Save(*sql.DB)
	Description() string
	// returns commands available for a given permission level
	Help(int) *discordgo.MessageEmbed
	// minimum permissions needed to use base plugin
	MinimumPermissions() int
}

// RegisterPlugin allows the plugin to register itself in the bot.
// Should be called in the init func of each plugin you want hooked
func RegisterPlugin(plugin Plugin) {
	plugins = append(plugins, plugin)
}

// GoFumo is a wrapper around the discordgo session
type GoFumo struct {
	dg *discordgo.Session
	db *sql.DB

	hasOpened bool
	hasClosed bool
	lastError error
}

// New creates an instance of GoFumo bot with provided token and loads plugins that have been initiated
func New(t string, keys map[string]string) (*GoFumo, error) {
	log.Println("Initializing")
	Prefix = "!"
	dg, err := discordgo.New("Bot " + t)
	if err != nil {
		return nil, err
	}
	dbFile := "./bot.db"
	db, err := sql.Open("sqlite3", dbFile)
	if err != nil {
		return nil, err
	}
	gf := &GoFumo{
		dg: dg,
		db: db,
	}
	// call this before doing anything with plugins to load constants
	makeEmbedStuff()

	log.Println("Loading plugins")
	for _, plugin := range plugins {
		log.Printf("  %s", plugin.Name())
		// let the plugin save connection to db or just use once if needed
		plugin.Load(db, keys)
		// load each command's handler
		for _, command := range plugin.Commands() {
			gf.dg.AddHandler(command.Handler())
		}
		// Create the helper function and stuff
		gf.dg.AddHandler(makePluginHelpHandler(plugin))
	}

	gf.dg.AddHandler(helpHandler)
	return gf, nil
}

// we make this once since that's all we need really
func makeEmbedStuff() {

	EmbedAuthor = &discordgo.MessageEmbedAuthor{
		IconURL: "https://i.heart.lolisports.com/marv/YjAzN.png",
		Name:    "GoFumo",
	}

	EmbedColor = 0xff0000
}

func ContainsPermission(actual, want int) bool {
	matching := actual & want
	return matching == want
}

func helpHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !strings.HasPrefix(m.Content, Prefix) {
		return
	}
	if m.Content[1:] == "help" {
		embed := &discordgo.MessageEmbed{
			//Title:       "GoFumo",
			Description: "Use \"!<module> help\" for more information",
			Author:      EmbedAuthor,
			Fields:      []*discordgo.MessageEmbedField{},
			Color:       EmbedColor,
		}
		for _, plugin := range plugins {
			userPerms, _ := s.UserChannelPermissions(m.Author.ID, m.ChannelID)
			minPluginPerms := plugin.MinimumPermissions()
			if !ContainsPermission(userPerms, minPluginPerms) {
				continue
			}
			field := &discordgo.MessageEmbedField{
				Name:  plugin.Name(),
				Value: plugin.Description(),
			}
			embed.Fields = append(embed.Fields, field)
		}
		_, err := s.ChannelMessageSendEmbed(m.ChannelID, embed)
		if err != nil {
			log.Println(err)
		}
	}
}

func makePluginHelpHandler(plugin Plugin) func(*discordgo.Session, *discordgo.MessageCreate) {
	// cover both incase people use one or the other
	help1 := fmt.Sprintf("%shelp %s", Prefix, plugin.Name())
	help1 = strings.ToLower(help1)

	help2 := fmt.Sprintf("%s%s help", Prefix, plugin.Name())
	help2 = strings.ToLower(help2)
	return func(s *discordgo.Session, m *discordgo.MessageCreate) {
		msg := strings.ToLower(m.Content)
		if msg == help1 || msg == help2 {
			// pull perms
			userPerms, err := s.UserChannelPermissions(m.Author.ID, m.ChannelID)
			if err != nil {
				log.Println(err)
				return
			}
			minPluginPerms := plugin.MinimumPermissions()
			matchingPerms := userPerms & minPluginPerms
			// if user doesn't have min perms needed to use any command, don't bother
			if minPluginPerms != matchingPerms {
				s.MessageReactionAdd(m.ChannelID, m.ID, EmojiCrossMark)
				return
			}
			// gen help page for perms given
			help := plugin.Help(userPerms)
			s.ChannelMessageSendEmbed(m.ChannelID, help)
		}
	}
}

// MakeEmbedForPlugin returns a basic embed for the help command
func MakeEmbedForPlugin(plugin Plugin, userPerms int) *discordgo.MessageEmbed {
	embed := &discordgo.MessageEmbed{
		Title:       plugin.Name(),
		Description: plugin.Description(),
		Author:      EmbedAuthor,
		Color:       EmbedColor,
		Fields:      []*discordgo.MessageEmbedField{},
	}
	// go through each main command
	for _, cmd := range plugin.Commands() {
		// check perms to see if we should skip it
		commandPerms := cmd.PermissionsNeeded()
		matchingPerms := userPerms & commandPerms
		if commandPerms != matchingPerms {
			continue
		}
		// now grab the sub ones and create the field/values
		subCommands := cmd.Commands()
		subHelps := cmd.Help()
		for idx := range subCommands {
			commandStr := subCommands[idx]
			helpStr := subHelps[idx]
			field := &discordgo.MessageEmbedField{
				Name:  commandStr,
				Value: fmt.Sprintf("*%s*", helpStr),
			}
			// prepend if needed
			if cmd.RequiresModulePrefix() {
				field.Name = fmt.Sprintf("%s %s", plugin.Name(), field.Name)
			}
			// prepend if needed. This always goes first
			if cmd.RequiresCharPrefix() {
				field.Name = fmt.Sprintf("%s%s", cmd.CharPrefix(), field.Name)
			}
			field.Name = strings.ToLower(field.Name)
			embed.Fields = append(embed.Fields, field)
		}

	}
	return embed
}

// Open connects the bot to discord server
// It's also idempotent
func (gf *GoFumo) Open() error {
	if gf.hasOpened {
		return gf.lastError
	}
	log.Println("Connecting")
	err := gf.dg.Open()
	gf.hasOpened = true
	if err != nil {
		gf.lastError = err
		return err
	}
	return nil
}

// Close disconnects the bot from the discord server
// Runs save function for each plugin
// It's also idempotent
func (gf *GoFumo) Close() {
	if gf.hasClosed {
		return
	}
	log.Println("Saving Plugins")
	for _, plugin := range plugins {
		plugin.Save(gf.db)
	}
	log.Println("Closing")
	gf.dg.Close()
	gf.hasClosed = true
}
